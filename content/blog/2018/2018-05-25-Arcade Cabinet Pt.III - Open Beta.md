+++
author = "dotMavriQ"
title = "Nördmässan"
date = "2018-05-25"
description = "The Arcade Cabinet pt.III"
tags = ["Makerspace", "Life"]
categories = ["Blog"]
aliases = ["2018-05-25-nordmassan"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-05-25-MS.jpg"
alt = ""
stretch = ""
+++

Waking up was tough today for some reason.. didn't even get proper food going before it was showtime. 

I got to MakerSpace, carried down the cabinet with the boys and made sure that our spot was saved. We got a bit crammed next to the VR-dudes but settled in nicely. 

A LOT of people played our machine, we had to run coins repeatedly into the slot so as to bank up continues for more and more people to try it. 

Me and Alex had to leave for a party at David K's later that night so we managed to have people go get beer while we attended.
Might write about how that went tomorrow or something.

I.. must say I thought the event was gonna be a bit more sizeable, not that I'm complaining, I actually think this was a really good way for me and the boys to test out cooperation, interaction.. all that good stuff. 

We did do some networking as well, talking to Mattis, an old friend of mine, he shook hands with us and promised that he'd do the paint job for the cabinet which makes me really happy as he's talented with that stuff. We also talked to Nelly who makes her own comics, we might do some stuff together in the future for sure. 


Saddest moment was probably just being struck by hunger and fatigue only to be met with well meaning kids kindly asking us to start the VR for like the fifth time hahah.. but yeah the people that work with LDF and Con made the experience an all in all nice one, here's to hoping there will be more so that MakerSpace can show its face again and actually get some creative minds to join in! 

Over and out
