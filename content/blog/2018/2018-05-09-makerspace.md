+++
author = "dotMavriQ"
title = "Makerspace"
date = "2018-05-09"
description = "The Arcade Cabinet pt.II"
tags = ["Makerspace", "Life"]
categories = ["Blog"]
aliases = ["2018-05-09-makerspace"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-05-17-makerspace.jpg"
alt = ""
stretch = ""
+++
I started today with waking up (weird, I know), getting my flat ready for a well needed spring cleaning and then I talked to Margarida on Skype for a bit. 

We originally intended to begin working on the cabinet last night but because of tool shortages we had to delay it until today.

I end up calling up Phil, he wanted to finish some important stuff at work so I gave the guy some time.
Two hours later he calls me back, I call Alex and they.. sort of haven't progressed... like.. at all.

> We tried but we can't get in even though we have a key, someone's locked it down and we need a Tag..

Turns out they can't get into MakerSpace because *none* of them have **tags** and well, someone over at *Sockerbruket* had locked down the entire **C-section** which happens to be the place where MakerSpace recides.

Oh geez.. I decided that the day finally begins with a *Super-Roll* (A kebab roll with fries, also known as a Viking Roll) over at [Lidköpings Pizzeria](http://lidkopingpizzeria.se/), the best pizzeria in Lidköping! 

Alex then calls me and says he'll show up with the tools, what a trooper! 

Halef hands us the rolls, joy can be restored to life..and off we go to the MakerSpace.
Alex shows up after some time, having carried the powertools we graciously got to borrow from Sebastians father. 
Now the work starts! 

So we have bought lengths of wood with which to create the structure base for the cabinet, it will need more than just your usual strengthening as a screen will be mounted inside of it and so on and so forth.

We started by cutting up the lengths to appropriate proportions, and by the end of the day we assembled the pieces with a screwdriver.

Over and out.
