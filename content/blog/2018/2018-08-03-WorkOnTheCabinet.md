+++
author = "dotMavriQ"
title = "Work On The Cabinet"
date = "2018-08-03"
description = "Cabinet Part Four"
tags = ["Life", "Makerspace"]
categories = ["Blog"]
aliases = ["2018-08-03-WorkOnTheCabinet"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-08-03-WorkOnTheCabinet1.jpg"
alt = ""
stretch = ""
+++
  
Went to Makerspace today to get some more work done on the cabinet.

There have been several delays in the project as me and Phil started to focus on expanding the operation and dealing with all of paperwork, promotional work, recruitment work and everything else involved with running an NGO.

A lot of our other board members have unfortunately had their hands tied for a while and so work has staggered a little bit lately.

But now we're back on track! 

Me and Phil installed the door and did a lot of sanding and filing.

There's not a whole lot left now! 

Some wood fix, securing of the screen-strap, further sanding and filing...paint job... And we're done! 

I can't wait to see it when it's done!


---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-08-03-WorkOnTheCabinet1.jpg" gallery="gallery" caption="Makerspaaace" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-08-03-WorkOnTheCabinet2.jpg" gallery="gallery" caption="Makerspaaace" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-08-03-WorkOnTheCabinet3.jpg" gallery="gallery" caption="Makerspaaace" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-08-03-WorkOnTheCabinet4.jpg" gallery="gallery" caption="Makerspaaace" >}}
