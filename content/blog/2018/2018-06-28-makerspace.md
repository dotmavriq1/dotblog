+++
author = "dotMavriQ"
title = "Swinging by Makerspace"
date = "2018-06-28"
description = "Humble beginnings"
tags = ["Life", "Makerspace"]
categories = ["Blog"]
aliases = ["2018-06-28-makerspace"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-06-28_ms2.jpg"
alt = ""
stretch = ""
+++

Had an easy day with nothing special to report until the clock struck *17:30* and it was time to head to *MakerSpace*. 

It's been a continued success with the smaller moves that we have made.

From having started the endeavour with Phil just a couple of weeks ago I have a lot of good friends and eager minds joining me in creating something for my home town that I have felt has been sorely missing throughout my youth.

We went to the MakerSpace HQ to check in on the new computer that was kindly donated to us by [Anders](https://se.linkedin.com/in/anders-wik-998a8255). 

I also have this mockup for the posters that we plan on putting up all over town in an effort to advertise so that anyone that seems interested knows that we exist.

yeah.. that's about it for today I think, the other guys had other obligations for the day, might get a bit more crazy and creative in the coming weeks, we'll see! 

Over and Out 
