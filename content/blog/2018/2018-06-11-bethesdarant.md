+++
author = "dotMavriQ"
title = "Fallout 76"
date = "2018-06-11"
description = "I have a bad feeling about this one"
tags = ["Rant", "Games"]
categories = ["Blog"]
aliases = ["2018-06-11-fallout"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-06-11-fallout76.jpg"
alt = ""
stretch = ""
+++

Nukes? since when? 
Ugh... 
A worse-than-DRM-lock business approach where modding won't be possible. Servers for years? Yeah? And then what? Fallout taking a step into planned obsolescence? 
People still play Fallout 3, the idea of good mechanics, a pleasant storyline, tons of choices, freedom, aesthetics.. moddability... It's a game for the ages. Fallout 4 tried new things in a sense, but fundamentally dropped on everything that gave it staying power.. you made it Mass Effect itself into public disinterest.
This is why Half-Life 3 isn't being made.

Not a single game got a strong focus on story, it's product pitch, I get it.. but what made Wasteland 2 work was confidence, what makes people play games that are covered in cheese and crust is unorthodox non-best-practice approaches and most of all SUBSTANCE! Please don't play too big to fail, this pitch felt like Bethesda went Apple more than anything else.. loved the flexing on the money too like; "Mobile game made us more money than all other games combined".. there's no other way to read into his language other than "We need to somehow talk about this people, I know you own the culture but we own what you consume so here's the pitch, swallow it".. as cynical as I sound saying it that was my first reflection and I still stand by it. 

And in regards to Blades...
**My positive takes are these:** 
* thank God it's not a MOBA, an arena shooter or a tired card game. 
* A cell phone is strong enough to emulate games up to N64 with confidence, and further if the emulation community had more people. 
* People absolutely love games, they love continuity and different people like different games. 

This "Pareto-distribution, larger-than-life franchise extravaganza" that now persists in Hollywood and triple-A gaming are most surely doing more harm than good, and people have been screaming about it for years! 

You know what I'm talking about... Fan service... movies directed more by boards than directors... 
It's ruining art!

## How about daring something bigger..

Lock assets, physics, and dialog-functionality and drop PC/MAC "SDKs". 
Somewhere between open sourcing with license and Mario Maker, software that lets people create their own story with the mythos you have been big enough to create. Imagine a mass market Unity engine with a program that lets EVERYONE engage with varying degrees of complexity, that encourages learning, that creates worldwide collaboration never before seen. 

We saw it with G-mod, saw it with Minecraft.. hell even Roblox I guess.. and the Streamers almost exclusively play other people's creations, look at PewDiePie's marketing scheme; Have a game created that acts like the game that gave him the most views, brand it, seal it, deal it and ultimately engage with it like I once engaged with the original product.. even attempting it, with irony and distance and supposed maturity of course.. still brings it home doesn't it?

Godhood.

I guess a lot of why it hasn't happened is "How do we keep our brand intact when people seek out to harm it".. PC sensibility and what not... I get it. 

But .. at least start THINKING in this direction. Mobile gaming has been dogshit for the longest time. And let's not go away from the fact that you're not interested in the mobile market any more than you are interested in means of stimulating microtransactions. Everybody has a phone, and it's an easier market to exploit.

**Glorified Gambling**. Same goes with the abomination that is Lootboxes... *ugh*

No.. you people really should invent these platforms.
..Letting creators pay for assets, making assets and you earning money from other people buying it in percentages.. hell why not license it so you can use other peoples creations if fans stick with some of it. You are already out to replace God, at least act in accordance, THAT would be Syntheos!

Minecraft didn't win because of arbitrary functions in the game like looting, procedural generation, rouge-light or the graphics. It won because Java allowed it to be played EVERYWHERE with little effort and modded with less, the programmers of the future started with Minecraft ( Old fart here, I started with Game Maker ).
The fact that Minecraft's team was indie made so that they didn't cry when someone made a nudity mod or worse, and the fact that triple-A have brands outdoing nations economically and the proposed responsibilities that come with it might be what is harming the REAL revolution in gaming, I think people feel it. The waste of potential, the inertia from the Gods. 
More on these thoughts some other time...

Oh and Thank you for reading my rants btw.. geez.. I know I'm getting ahead of myself.. back to Bethesda.

Bethesda has survived to a LARGE degree thanks to the morbidly ambitious modding community. 
Skyblivion ought to be the be-all-end-all proof that I'm in the right.
Please don't fuck your fans over by going hardcore with things that will fundamentally erase the very support systems that have kept your consumers alive and hungry for more. 

The only thing more harmful than DRM is Forced Online. You dropped a game so ambitious that you sensibly even drop it as beta out of fear of blowing too hard as it drops, that's pro-gamer thinking right there. Yeah, I know.. you damage controlled.. I know you said it would be "solo-possible" but the focus is clearly dropped and I'll be damned if that doesn't manifest clearly in the end product just like how Fallout 4 was an attempt to outcompete others with mimicry of proven practice. Worked great for the conversation system right? 
And not everyone wants to craft, it certainly doesn't play to old strengths in the franchise... 

Hahaha... TES: VI dropped in there like you had more confidence than Kanye.. wow.. that was.. nothing short of insecure.

And what about the modders? What about the ability for others to host?
Will it come? Or will it just perish to whatever fate consumers give it the first run.. if that's the case it saddens me that you are creating products you don't even believe in yourself. 
