+++
author = "dotMavriQ"
title = "Lödöse Viking Market"
date = "2018-06-02"
description = "A fun Viking-themed roadtrip with Phil"
tags = ["Life", "Travel"]
categories = ["Blog"]
aliases = ["2018-06-02-vikings"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-06-02-LödöseVikingMarket1.jpg"
alt = ""
stretch = ""
+++

I really wish I would've gotten more sleep, but it is what is and so I got ready and Phil came to pick me up.

I made sure to water the garden early, I do have things to maintain.

Got in the car, the heat was almost crippling from the get-go but Phil's car has a really nice cooling system that kicked in after a little while.

We scavenged the nearby ICA Supermarket and bought breakfast and then we headed off to **Lödöse**.


The trip went nice, I've always been one for travel and Sweden is stunning this time of the year.

One thing though.. I noticed how incredibly selective Google Home is with picking up songs to play through the cars internal Spotify-system.

So many songs I wanted to listen to.. and the Spotify interface was too road safe to even allow me to search properly and the voice function didn't pick the damn songs I wanted to hear, didn't matter if it was me or Phil asking the same thing.

Some might be censored for [PC](https://en.wikipedia.org/wiki/Political_correctness) reasons I guess.. wouldn't want Google VUI accidentally playing [xAxCx](https://open.spotify.com/track/2xx7b4eug8nXaQkiBm10RT?si=DHUQjCriRiOtn8fvodLMUw) for the kids, *right*?

  

We eventually settle outside of **[Lödöse museum](https://www.fotevikensmuseum.se/d/en/vikingar/vikingamarknaden/2018)** and after a really gratifying visit to the Water Closet, it was time to pay 100kr to enter.

  

**I'll try to summarize the visit briefly:**

  ---

We started off by circling the place while it was still being set up, most things were up but the vitality wasn't there and I blame the weather for a lot of it. They had arts and crafts with medieval and Viking themes, really nice organic foods like jam and bread and honey caramels.

  

The most impressive thing to me was an old-school Smith who stood there next to his Bellow explaining old techniques.

  

Me and Phil also tried archery and I still have this fool's luck with arching that feeds my drive to want to pursue it more.

My last shot landed on the corner of the Target in a way that impressed even the guy who stood at the arching booth.

  

Phil also tried throwing Axe and it was cool, the last try was so close to putting in the block of wood but that stuff takes practice... And a sharper axe, *I think*.

We ended up falling short on time so we visited the museum for a little bit, read up on history, walked through the garden .. I saw **Kvanne**! (which I've always been intrigued to eat after a perverted look into my lineage showed me that I am part Walloon and Walloons apparently eat candied [Kvanne](https://en.wikipedia.org/wiki/Angelica_archangelica).)  

We ended our little road trip with a stop by the Military Surplus store. It's a really cool store and love what they do but I guess the fact of the matter is that we don't have as much "*cool"* surplus going as when I was a kid so things are a bit expensive these days, at least for my budget, I ended up not buying anything and Phil bought a mallet.

The ride home was neat, we ate at **Max** Hamburger Chain which is a Swedish franchise and boy do they slay McDonalds and Burger King *by miles*. **The Frisco Burger** was just a tiny improvement in dressing away from being something I would miss to have around on a regular basis, it's tasty fast food, and I'm usually against all that stuff (franchise fast food that is).

Once home I carried the vacuum cleaner through the door, had some cookies and smoothie and then I slept for **FOUR HOURS**. Upon waking up Phil had already had dinner and well... I had food at home so I just decided to relax.

Tried playing **Wasteland 2** for a bit but my graphics card is *too beat up*... It's depressing, I really need to put decent money down on a better graphics card right now.. ~~ugh~~.

I then watched [Anders Photoshop stream](https://www.youtube.com/user/TheWikTube) which was pleasant, he's a really talented artist and deserves more support!

And now.. as I'm writing this, I end my day by talking to Margarida. It's the only good way to end a day.

MakerSpace tomorrow, can't wait!

Good night people.

---

##### Images taken throughout the day:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-06-02-LödöseVikingMarket1.jpg" gallery="gallery" caption="Phil throwing axes... I decided not to try... but I did try archery!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-06-02-LödöseVikingMarket2.jpg" gallery="gallery" caption="Lödöse Viking Market" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-06-02-LödöseVikingMarket3.jpg" gallery="gallery" caption="Lödöse Viking Market" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-06-02-LödöseVikingMarket4.jpg" gallery="gallery" caption="Lödöse Viking Market" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-06-02-LödöseVikingMarket5.jpg" gallery="gallery" caption="Lödöse Viking Market" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-06-02-LödöseVikingMarket6.jpg" gallery="gallery" caption="Lödöse Viking Market" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-06-02-LödöseVikingMarket7.jpg" gallery="gallery" caption="Lödöse Viking Market" >}}
