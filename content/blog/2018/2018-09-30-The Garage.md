+++
author = "dotMavriQ"
title = "The Garage"
date = "2018-09-30"
description = "We are expanding!"
tags = ["Makerspace", "Life"]
categories = ["Blog"]
aliases = ["2018-09-30-TheGarage"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace/Makerspace25.jpg"
alt = ""
stretch = ""
+++

After some negotiations with [Ung Arena Lidköping](https://ungarena.nu/) we finally landed a deal where we would have access to a garage, brim-filled with equipment for car maintenance and the likes.

We are very happy that they have placed confidence in us to use the resources they've got in order to pursue creative endeavors and welcome any new member to join us.

It felt wonderful to accept the keys so we could finally check it out. 

We have already put plans in place to disperse ourselves throughout our weekly schedule so that our members will have access to the tools inside of the garage.

We also got a microwave and a coffee machine, so join us for the simple yearly fee of 100kr and we'll hook you up with some brew and the tools you need to realize your dreams!

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace21.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace22.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace23.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace24.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace25.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace26.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace27.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace28.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace29.jpg" gallery="gallery" caption="Makerspace Photos" >}}
