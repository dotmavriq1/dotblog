+++
author = "dotMavriQ"
title = "Garden update"
date = "2018-06-25"
description = "It's looking pretty good if you ask me"
tags = ["Life", "Garden"]
categories = ["Blog"]
aliases = ["2018-06-25-Garden"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-06-25-gardenupdate.jpg"
alt = ""
stretch = ""
+++

**So here's a short little garden update:**

Sorry for not being that active with the updates on this... Life has been quite busy lately...in a good way though! 

[Here's my last post if you haven't checked it out!](https://blog.dotmavriq.life/blog/2018/2018-06-25-garden-update/)

**Anyway, quick update:**

* **Rhubarb** seems alive and well
* Holy shiz I can't believe the Portuguese **Butternut** I planted with seeds salvaged from one I bought in a supermarket is growing and doing well.
* **Red Basil** is doing good.
* **Chives** seem to be a bit rough, should tend to it more.
* **Thyme** is nice and going strong.
* Well hello there **Salad**! 
* **Radishes** galore! 
* **Strawberry plants** are alive, no sign of strawberries though..
* My **Penis Chili**-plant is growing, no ehrm.. fruits on it yet though.
* **Ramson** is hanging in there..I guess?

yeah.. I've been a bit lazy with it lately, I'll make sure to shape it up soon enough though.

Over and out.

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-06-25-gardenupdate.jpg" gallery="gallery" caption="My cute little garden!" >}}
