+++
author = "dotMavriQ"
title = "We are now open!"
date = "2018-04-16"
description = "Feel free to join Makerspace!"
tags = ["Makerspace", "Life"]
categories = ["Blog"]
aliases = ["2018-04-16-Makerspace"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace/2018-04-16-Makerspace1.jpg"
alt = ""
stretch = ""
+++

Today we are opening the doors for members to join us during our regular scheduled opening hours! 

We're starting with a humble promise of keeping the door open at Thursday every week between 17:00 and 21:00.

##### We are a Makerspace with humble beginnings and a lot of ambition.

We are ideologically driven to create an organization that gathers the tools, machines, knowledge and expertise needed for our members to realize their creative dreams but also to learn more about the technology that we are surrounded with in society, how to repair things and hopefully how to pursue technology in a way that is more favorable for humans.

Humans shouldn't live under the subjugation of the technology that society now tells us that we need, and with the trajectory that the world has been taking lately we are simply not keeping up, and that is a slow but very harmful threat to a democratic and free society.

Makerspace Lidköping sets out to be the uniting cultural force of emancipation.

We value freedom, democracy and we are pro-Open Source software and hardware solutions. 

Our grandest ambition is to become a strong contributor to these technologies at some point.

But if you're just out to see if you can fix your damn alarm clock this is still the place to be, our agenda really doesn't stretch much further than that in actuality... we just want to help people do what they seemingly never find the time or the resources to do themselves.

And we believe in this.

We believe that society needs it.

And we can see it all over the world with other Makerspaces and incentives like the MIT-licensed Fab Labs that are popping up all over America and Europe.

And we have of course, at this early stage, not acquired big machinery or anything like that... but we are starting to pool together donations from members and people that believe in our cause and we will continually fill up our stash of tools!

...Oh and we also just got a coffee machine and some computers to tinker with!

##### Our first collaborative project!

We can now announce that our first Team effort will be to create a commisioned Arcade Cabinet for Dataspelsföreningen.

A classic wooden booth, screen, it should take coins and operate with classic arcade controllers.

We are so stoked about this! 

I'll be sure to post updates!

Join us, you won't regret it!

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="2018-04-16-Makerspace1.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="2018-04-16-Makerspace2.jpg" gallery="gallery" caption="Makerspace Photos" >}}
