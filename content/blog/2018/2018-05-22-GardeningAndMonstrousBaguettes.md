+++
author = "dotMavriQ"
title = "Gardening and Monstrous Baguettes"
date = "2018-05-22"
description = "It's looking pretty good if you ask me"
tags = ["Life", "Garden", "Baguette"]
categories = ["Blog"]
aliases = ["2018-05-22-GardeningAndMonstrousBaguettes"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-05-22-GardeningAndMonstrousBaguettes1.jpg"
alt = ""
stretch = ""
+++

One of the things that made me really happy about moving into my appartment was the fact that I was offered the possibility of having my own little allotment to garden in. 

It's very humble and all that but it's enough to get a decent yield for anyone that knows what they're doing.

I barely...at least I still consider myself to be someone that is actively learning it.

It's not my first time! 

I actually had one when I was a kid together with my mom and dad... I remember failing with paprica but seeing some moderate success with other things... it's even more funny that we used to have *that* allotment just across the street from where I now live hahah!

Lidköping *is* a small place after all.

---

##### Quick Garden update:
* The Rhubarb is overbloomed... some of the stems are hardened into flower-stems and can't be eaten.
* The Chives look good though!
* Herbs haven't survived since I planted them... but that wasn't too bad if you ask me... it was kinda expected.

So I should probably plant as much stuff as I can while the sun is still out... I'll be sure to keep you guys posted!

---

Later during the day I met up with Phil and we pursued our hobby of making the most obnoxious baguettes possible.

We started with a *Pre-Dinner Snack* which was a *Toscabulle*, a cinnamon roll(Swedes call them Kanelbulle) that has been replaced with filling for another Swedish classic, namely the *Toscakaka*... It was alright...

Back to the Baguette.

**This time it featured:**
* Ruccola salad
* Wienerschnitzel
* Goatmilk Mac'n'Cheese
* Jalapeños
* Bacon and *Dorito Bites* 

... It was ehrm... It was actually tasty I'm not going to lie...but no man should ever consume that amount of carbs in one go.

I have first hand account of that...*God*...

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-22-GardeningAndMonstrousBaguettes1.jpg" gallery="gallery" caption="My cute little garden!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-22-GardeningAndMonstrousBaguettes2.jpg" gallery="gallery" caption="Toscabulle" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-22-GardeningAndMonstrousBaguettes3.jpg" gallery="gallery" caption="Look at that BEAST" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-22-GardeningAndMonstrousBaguettes4.jpg" gallery="gallery" caption="This should be a punishable offense" >}}
