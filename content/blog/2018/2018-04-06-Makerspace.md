+++
author = "dotMavriQ"
title = "Moving in to Makerspace"
date = "2018-04-06"
description = "Got ourselves some couches and pizza too!"
tags = ["Makerspace", "Life"]
categories = ["Blog"]
aliases = ["2018-04-06-Makerspace"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace/Makerspace18.jpg"
alt = ""
stretch = ""
+++

So today we're finally moving into our new Makerspace. 

We managed to land a deal with so that we can have access to a classroom inside of Sockerbruket here in Lidköping.

It will grant us enough privacy so that we can be noisy during certain hours, it gives us access to electricity and everything we need in order to get this organization off of the ground. 

Me, Phil and Alex met up and got a hold of a van and started making calls for people that no longer wanted their furniture and traveled to a nice couple somewhere in the outskirts of town. 

Other furniture that we have placed out have come from either Phil, me or Alex... 

One brick at a time.

You're all welcome to join!

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace12.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace14.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace15.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace17.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace18.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace19.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="Makerspace20.jpg" gallery="gallery" caption="Makerspace Photos" >}}
