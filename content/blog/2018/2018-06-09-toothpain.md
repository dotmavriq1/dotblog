+++
author = "dotMavriQ"
title = "Tooth pain"
date = "2018-06-09"
description = "Guess it's just podcasts today"
tags = ["Sickness", "Life", "Teeth"]
categories = ["Blog"]
aliases = ["2018-06-09"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/dentist.png"
alt = ""
stretch = ""
+++

A bit rough morning, woke up around 10 with my wound from yesterday's tooth-removal even more pronounced than yesterday in terms of swelling, and I can sort of feel the place where the stitches are. 

To shed some well needed light in the darkness: at least I can't chew on the stitches, I didn't chew on my tongue when I had the anaesthesia, I got sleep and I can't really say that my pain is great today except for parts of eating that I still struggle with unfortunately. 

Today is a day of healing.. so a lot of catching up with the public intellectuals, anything I've missed.. podcasts.. books to the extent I can.. and some well needed cleaning. 

I think I'll finish today off with some movies. 
