+++
author = "dotMavriQ"
title = "Hamsterposting"
date = "2018-06-10"
description = "And no, I am not storing nuts in my cheeks at the moment"
tags = ["Sickness", "Life"]
categories = ["Blog"]
aliases = ["2018-06-11-fallout"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-05-10-hamster.jpg"
alt = ""
stretch = ""
+++

Hello everybody, Hamster here! 

Or at least on one half of my face. 
I think it's healing up pretty ok though, although I will have to admit that I've already grown tired of the "Healing" thing... like.. only being able to eat soft foods without experiencing pain and all that, but I'll be ok. 

Last night I bought and played The Saboteur from GoG.com. It's a classic game I never got the chance to play and I love the style of the game as well as the story. The mad discount convinced me it was worth it, haha! 

I will have to play it some more to leave a more proper review.  

Tomorrow I think I'll write a piece on the Bethesda E3 showcase and the impressions I got from that... I guess video games is what I deal in when I'm too messed up to bother with much else.. I have had a sharp headache since the tooth removal and not even high amounts of painkillers seem to help...

I've stayed hydrated and took some multivitamins last night but it's still gnawing at me so I'm basically stuck with podcasts, sleep, soft foods, movies and video games for the time being. 

Revisited the Vikings TV-series but it mostly just pissed me off with all its inaccuracies weighing over the entertainment I get from it. I used to really like it.. guess I grew out of that now as I'm finishing off the fourth season. 

Yeah... That might just be it for today.

Stay weird folks. 
