+++
author = "dotMavriQ"
title = "Teaching Kids How To Solder"
date = "2018-12-04"
description = "A Makerspace Event where we teach kids to get into electronics!"
tags = ["Makerspace", "Life"]
categories = ["Blog"]
aliases = ["2018-12-04-TeachingKidsHowToSolder"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace/2020-12-04-TeachKidsHowToSolder1.jpg"
alt = ""
stretch = ""
+++

In collaboration with [**Ung Arena Lidköping**](https://ungarena.nu/) we got the chance to have a little space where we teach kids some basic electronics for Christmas. They provided us with some 200 blinking LED trees and stars et. c. 

Different shapes and circuits for the kids to pick between, we also ranked them from easy to hard as we had to do some prep ourselves and make sure that we understood what we were doing.

We set up soldering stations and brought the whole Makerspace board over to teach as many kids as we can.

The truth of what happened was that not every single kid managed to finish their creation, *which was expected*, but everyone that tried seemed to have a great time, and we did what we could to save and salvage their blinking trees so that they got something to bring home.

There were like 5 kids that did it and went from zero knowledge to complete their tree, and they seemed to enjoy it.

We are very thankful that **Ung Arena** placed the confidence in us to use the resources they gave us so that we could do what we think matters, which is to teach children that learning doesn't have to feel sterile and happen in isolated instances through some institutions like school... That they have the power to teach themselves more about how the world works, especially from a technological perspective.

I already know that I will sleep like a baby tonight.

I am so proud of my team and with the vision that I have been pursuing together with Phil for almost a year now.

I don't regret a single second and I look forward to 2019.

Thanks to everyone that came out, young, old, and in between!

..oh and shoutouts to [Ianertson](https://www.youtube.com/c/HelloWorldCode/videos), an old friend of mine that used to work with me at [Duva](https://duva.nu/) he came from Gothenburg to check us out... you guys should check out his YouTube channel for some neat programming tutorials and whatnot!

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="2020-12-04-TeachKidsHowToSolder1.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="2020-12-04-TeachKidsHowToSolder2.jpg" gallery="gallery" caption="Makerspace Photos" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Makerspace" file="2020-12-04-TeachKidsHowToSolder3.png" gallery="gallery" caption="Makerspace Photos" >}}
