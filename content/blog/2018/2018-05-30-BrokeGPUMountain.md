+++
author = "dotMavriQ"
title = "BrokeGPU Mountain"
date = "2018-05-30"
description = "My graphics card died so I went for an adventure"
tags = ["Life", "Baguette"]
categories = ["Blog"]
aliases = ["2018-05-30-BrokeGPUMountain"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-05-30-BrokeGPUMountain4.jpg"
alt = ""
stretch = ""
+++

##### The Green screen of death

Yeah... it sucks but it seems like my graphics card is giving in.

I really wish I didn't have to bother with shit like this... but I have the money... I might as well stop complaining and look for a better one as soon as possible

I got bored and so I told my comerade Phil that maybe he'd be able to have a look at it... We did, he thinks I am correct in the fact that it is indeed screwed.

So we went on a little adventure in his car.

We bought candy, barbecue equipment, stuff for yet another one of our crazy baguettes and all of his HAM Radio Equipment.

We stationed ourselves at the top of *Råda Vy*, which is located at the top of Råda Sand, a couple of kilometers away from where I live.

The view was lovely... it was a tiny bit windy.

Phil set up shop with his radio equipment and I started grilling the stuff.

**This baguette has:**
* Two types of hot sausage
* Potato sallad
* Bearnaise...
* Ramsons(!!!)

It was quite lovely actually... I brought a book to read and my camping chair while Phil fiddled around with his radio stuff.

We heard a bunch of really weird faint radio stations and I believe at one point he intercepted a Portuguese man chatting over radio frequencies... really cool stuff! 

Over and out.

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-30-BrokeGPUMountain1.jpg" gallery="gallery" caption="The screen you never want to see..." >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-30-BrokeGPUMountain2.jpg" gallery="gallery" caption="Got some candy to deal with the loss of my graphics card" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-30-BrokeGPUMountain3.jpg" gallery="gallery" caption="The stuff" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-30-BrokeGPUMountain4.jpg" gallery="gallery" caption="Beautiful view atop Råda" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2018-05-30-BrokeGPUMountain5.jpg" gallery="gallery" caption="It kinda turned into this lovely little french hot dog thing!" >}}
