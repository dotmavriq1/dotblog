+++
author = "dotMavriQ"
title = "Fifth of June"
date = "2018-06-05"
description = "Not a whole lot going on to be honest"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2018-06-05-fifthofjune"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2018-06-05-mindlerfix.png"
alt = ""
stretch = ""
+++

Not sure if pollen allergies or just generally fatigued but I've been wandering today like somewhat of a zombie, or.. a contempt zombie in pain?..something like that.

One thing is for certain though, it is REALLY hot outside, bringing both the good and the bad aspects of that.

---

I saw an ad for [Mindler](https://play.google.com/store/apps/details?id=se.mindler.video), some sort of app for people with mental health issues. I saw that their Google App Store had information that was flawed and had them fix it, good deed of the day: **cleared!**

My garden seems to be doing well... that's good, I like that. Still haven't had a single strawberry though.. *might be the birds*..might be the neighbours kids but I'll keep it light with the Alex Jones accusations, *it's just strawberries*. 

My parents had me over for dinner and after a couple of days of toothache it sure feels good to finally be able to eat solid food again. 
I was served a burger on sour dough bread baked by my father with delicious coleslaw.

And now.. let us listen to some Kate Bush.
