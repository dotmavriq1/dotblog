+++
author = "dotMavriQ"
title = "The Ninja Mission"
date = "2017-11-11"
description = "Who is Mats Helge Olsson, what did he do and what is his legacy?"
tags = ["Mats Helge Olsson", "Life"]
categories = ["Blog"]
aliases = ["2017-11-11-ninjamission"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2017-11-11-ninjamission.png"
alt = ""
stretch = "horizontal"
+++

I woke up a bit mushy on Alex couch because walking home in the cold rain would've sucked, last night was nice *if I recall correctly*.

Here is where I remember that I'm gonna meet with Emelie to go to the solarium, but it was actually really comfy and felt good, can't say I became magically brown after one sitting though, haha.

Going for a walk with Emelie was nice as always, we talk about philosophical stuff, trivial stuff.. anything you could imagine.

#### Spent the bigger part of saturday hanging out with Phil.

At first I thought we would be coding away but we ended up just talking, joking and watching stupid shit on the internet until late.

I have got to say that rewatching the first episode of the new [Grotesco](https://www.svtplay.se/grotesco) as well as the second episode really convinces me that this season is something for the history books.

Not only do they very intelligently critique this odd mental landscape that most people find themselves in today, it reveals the hypocrisy within all of us.

I think that a lot of Swedish people that are currently raving on about *"what the point of the TV-tax is"* sees this as the clear reason why it makes sense.

I do hear [Aron Flam](https://www.facebook.com/aron.flam/posts/10159552743365427?pnref=story) in his critique of this season though, there are points to be made about actually pointing a finger somewhere instead of mocking phenomena but more or less commiting the same crime in doing so without establishing a clear culprit, which is really tough to do if you want to be portrayed as apolitical but topical at the same time.. I'm getting ahead of myself.

The thing that really really did blow my mind however was when Phil casually mentioned a man named [Mats Helge Olsson](https://en.wikipedia.org/wiki/Mats_Helge).

Apparently there is this movie maker that had a movie studio just a couple of meters away from where we work in Lidköping that made movies that did end up making it big overseas. 

Swedish style Exploitation movies in the height of the 80's... I had no idea! 

We watch the documentary movie and this completely blows my mind, I can't wait to speak to everyone I know about this. 

I'm 25 years old and I've never heard of it, that's crazy!

Watching the movies you realize that they have this amateurish, cheesy C-movie quality to them but the actors where internationally known and one movie in partical, **The Ninja Mission**, did REALLY good overseas.

{{< youtube lSPjbi3x9F8 >}}

###### Later, when I talked to my dad about this, he had an interesting anecdote about it:

Apparently, when he was young, and after a night of heavy drinking, he wakes up, goes to the window and sees Ninjas jumping around in the bushes behind his appartment. 

And that was right in the middle of filming the Ninja Mission. ![hahah!](https://github.githubassets.com/images/icons/emoji/unicode/1f605.png)

And my good friend Jonas apparently knows the guy that made the astonishing soundtrack to the movie... aah, it's a small world!

I also spoke to my most film-obsessed friends, Simon and Anders, and they were as stoked as I was and had lots more to share about it, seeing as how their old media-teacher knew MHO personally.

I would love to get my local cinema to play the movies made by Mats Helge at some point.

#### The legacy needs to live on!
