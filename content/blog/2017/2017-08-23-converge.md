+++
author = "dotMavriQ"
title = "In All of Us Lies Secrets"
date = "2017-08-23"
description = "Hidden inside our hollow hearts"
tags = ["Vinyl", "Metal", "Life"]
categories = ["Blog"]
aliases = ["2017-08-23-Converge"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram (631).jpg"
alt = ""
stretch = ""
+++

**Converge** is one of those bands that some of you might never understand.

You could also end up like me, thinking that they are one of your favourite bands!

The vocalist is howling like a possessed demon over incredibly technical yet organically performed hardcore with clear influences of metal. 

This type of hardcore music was first adressed as Metalcore but has later been declared Metallic Hardcore in order to differentiate from the latter scene and the style it presented. 

Converge comes from a very interesting time in Hardcore Punk history where styles merged and the fiercest combinations surfaced. Bands like Botch and Coalesce paved the way for bands like Converge and The Dillinger Escape Plan.

If you are ready for the emotional rollercoaster of a lifetime you NEED to give Converge a listen.

This new single release comes with two tracks, 
[**I Can Tell You About Pain**](https://open.spotify.com/track/5sYU2A8hh1up0MzY4nf1iH?si=kX0mjEpyQiaJ9Lw_GcWzyw) as well as [**Eve**](https://open.spotify.com/track/7hcQALrk9WBV2oWI7hCTwP?si=G6garousTU2vdZ1ipPOdGg), a 7 and a half minute complete annihilation, with a brooding beginning, foreboding devastating lyric and delivery. One of the more special tracks ever released by Converge.
I'm gonna go listen the hell out of this right now, and so should you!

---


In all of us lies secrets  
Hidden inside our hollow hearts  
Heaven was built by heathens  
There is no place for us 
Never was  
  
All seeing eyes won't let us see  
Truth and purpose
is meant to sting  
When the pain strikes
We crash to our knees  
This is the eve
Of Everything
