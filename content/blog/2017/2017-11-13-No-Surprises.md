+++
author = "dotMavriQ"
title = "“It’s a fire isn’t it?”"
date = "2017-11-23"
description = "Also known as the day my neighbors thought the entire complex was on fire."
tags = ["JBP", "Life"]
categories = ["Blog"]
aliases = ["2017-11-23-nosurprises"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2017-11-23-nosurprises.jpg"
alt = ""
stretch = "horizontal"
+++

I woke up having barely had any sleep and was met by my panic alarm, I quickly stuff some Snus under my lip, wash my face, my armpits, freshen up and head to work.

It was an ok day at a pretty good pace, I got stuff done but I did end up leaving work feeling unfulfilled which I hate.

Having shown up a couple of minutes late it only felt right to leave work giving 30-40 minutes extra and also accompanying my fellow programming buddy as he had a crack at some backend/PHP-related issues.

Some days I’m a man that sees the world as a set of problems in dire need of solutions, and when my quota of solutions don’t equate the problems I become a cranky little boy that has to swallow his pride and relax with a podcast and some food.

#### Time to walk home

I put on the episode from the [Jordan B. Petersson Podcast](https://jordanbpeterson.com/2017/10/episode-31/) where he's in a conversation with Camille Paglia on the future of society and the dangers of postmodernism, I had already seen and thus heard the [video version of it](https://youtu.be/v-hIVnmUdXM) but figured it would be good enough to make due for my walk home from work. 

The episode was long and became a nice way to just procrastinate cooking and adulting.

I live in an area more or less *dedicated* to facilitating immigrants, I am one out of 2 native inhabitants *afaik*.

I like it here and the experience has been positive, the municipality that arranged for these module flat solutions has been service-minded and kind.

{{< youtube VMGnrs99WEY >}}

**Strong exception being how the washing facilities are handled.**

I can honestly say that I have no further complaints and I would even say that I’d rather live here than in the city because with my experience in living in flats I have yet to hear a single drill, hammer, bass booming speaker, argument, car noises..not anything, I could hear the kids play outside when it was sunny but for the most part that was just pleasant and it increases my mood. I’m glad the kids around here are happy and engaging in play.

#### But living here is not like anything I'm used to.

I had a neighbor who I just found out isn’t living here anymore that knocked on my door on my sixth day living here to try to help me help him buy a used car on Blocket, it was odd but it left me feeling good and I did have my lights on and he looked ashamed that he had asked that late but he probably saw it as a really good deal and there was probably some degrees of desperation involved.

He later invited me over and gave me a beer in exchange for me looking at a Swedish car insurance. He was a kind dude.

#### Lack of support net

I tried to explain to him that I don’t have a car nor do I have insurance myself and that bureaucracy can be tough in Sweden but it didn’t get through so I called the municipality the day after and got in contact with a dude that works for a local insurance firm that knew Arabic and put the number next to an image of a phone and a clock to display what times they could call him and then I put that note on my neighbor's door, I was stressing to the store one day when he rushed to open his door just to thank me with a bright smile on his face, that kind of stuff is enough to kill any negativity in your life for a good couple of days.


#### Back to today,

I had just finished that episode and finally stopped being a cranky snot-nosed boy ready to make some uninspired carbonara for the umpteenth time since I moved out. I hear some knocks on my door and I’m going, to be honest, I’m willing to be a neighbor, a good one even, but I don’t care if you’re Swedish, of other origins, the Queen, the Pope or Rihanna; I am NOT opening my door to someone unannounced at night unless it is a complete emergency when I’m undressed like a bachelor preparing my late-night grub.

The knocking becomes ever more intense and parallel to this I also hear frantic running and knocking on the other neighbor’s doors, the talking outside intensifies, my brain suddenly intervenes: **“It’s a fire isn’t it?”**.

I open the door and there’s a younger girl, a boy and an older lady telling me that there is smoke emitting from the building, they look worried and in need of help.

I also see other people leaving their flats to investigate or warn other neighbors. I want to keep my wits about me as the adrenaline strikes so I tell the young man that I’ll call the fire department, as I do that I do see emissions of smoke and they did not make that smell up. It did smell like burning plastic, let’s just say my spidey-sense was tingling at this point.

The kind lady on the line took me through steps as I tried to make sense of where the smoke was coming from, I couldn’t give her a clear answer so I just told her what my neighbors said and that I too have seen the smoke and sensed the smell.

At first, a regular duty car shows up, I believe one fireman in civil clothing ran all the way there just to check. And then comes a complete firetruck with fully dressed and ready firemen, as I turn around I notice that my neighbors have *conveniently left me there to discuss things with the firemen and that the smoke isn’t that present anymore…. …. …. …..*

The Captain did sense smoke emitting from my closest neighbors flat, but the ventilation *could also belong to me*, I ask some questions.. they look bothered which is understandable as they clearly can’t see a source of smoke, nor any fire for that matter.

I told them all I could and one man assured me that I did the right thing.

And after the captain, himself went into my punk rock 1 room apartment with my disgusting heap of unwashed dishes he scoffs and gently tells me that they can’t see any fire hazard in the building.

And no sign of the neighbors afterward either.

It took some time for me to calm down, I didn’t know if I was angry or scared or anything.. just riled up, full of adrenaline.

I skyped Margarida and she did a really good job of calming me down, and then I sat down to write this.

I don’t think my neighbors necessarily overreacted, and I do feel genuinely respected knowing that they went out of their way to see if I was in my flat. 

One possible theory is that the flats are very small and thus warm air mixed with cooking fumes could be the culprit that solicited this reaction out of them, and you know what? 

**When I was there with them I thought that it did burn somewhere too.**

In a freak case of synchronicity, it so happens that my fire alarm beeped last night to the point of having to pull it’s batteries out, and I haven’t changed it yet.

But I will now.

This is my final fit

My final bellyache with

No Alarms and No Surprises

Please.
