+++
author = "dotMavriQ"
title = "StreamOut"
date = "2020-07-30"
description = "A list of tunes you miss because Spotify"
tags = ["Rant", "List", "Spotify"]
categories = ["Blog"]
aliases = ["2020-07-30-PunchingSpotify"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/punchout.png"
alt = ""
stretch = ""
+++

## The year is 2020

##### It was a long time since most of us even bothered with physical media, be it Vinyl, CD, DVD or Cassettes. 

Spotify has grown to amount a staggering **300 million paid subscribers**. 

There is no denying that digital music is the dominant means of consuming music these days, and maybe it's for the better.

It's *convenient*, for one.

But it does remove the cultural stratification that once came with purchasing records, more than anything, more than the differences in profit for the record producers of yesterday is the sense that services grow into monopolies. And once they have, they fundamentally alter how we consume music.

> *"Oh... It's not on Spotify?"*

It's not so much that we wouldn't want to consume records that don't belong on spotify, but rather that we forget records *because they're not on Spotify.*

So this is my attempt at honoring records that *as of now* still haven't been placed into the Spotify roster.
Maybe this will be some sort of series in the future... we'll see.

#### No.1 - Glassjaw - The Coloring Book EP

![The Coloring Book EP](https://img.discogs.com/pRcs_55aTNdOgNnIDNxT2RYjBr4=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-2799337-1301568589.jpeg.jpg)

The Coloring Book EP was a short but incredibly underrated effort released by one of the most innovative post-hardcore bands, [**Glassjaw**](https://en.wikipedia.org/wiki/Glassjaw), in early 2011. 

The fastest way I can sell it to you is that it's a lovechild of [**Deftones**](https://en.wikipedia.org/wiki/Deftones) and [**The Mars Volta**](https://en.wikipedia.org/wiki/The_Mars_Volta). 
From [Palumbos](https://en.wikipedia.org/wiki/Daryl_Palumbo) soaring crooning vocal and astonishing range to the various spacey and off-the-cuff riffage alongside some of the more addictive drum patterns I've heard on a rock-effort in a very long time... this is a complete beast of an EP and one of my favorite releases.

#### No.2 Rollins Band - The End of Silence

![TheEndOfSilence](https://img.discogs.com/w-WmnFtBNkBVroxPxCFxfN_seLU=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-1240206-1203181685.jpeg.jpg)

I have a strong affinity for the type of *"Alternative Rock"* that emerged out of the later part of the 1990s and on into the early parts of the millenia.

It was as if people knew that most had already been done and played out, the band constellation still existed as a public phenomenon and there was efforts put in place for acts to tour giant venues and release physical media.

It was an interesting breeding ground for some really original acts to form and, being a huge fan of [Henry Rollins](https://en.wikipedia.org/wiki/Henry_Rollins), former vocalist of hardcore punk gods [**Black Flag**](https://en.wikipedia.org/wiki/Black_Flag_(band)), I knew I needed to sit down and listen through the last touring band he bothered with before focusing more on his [authorship, spoken word-shows and speaking gigs](https://www.henryrollins.com/).

[Rollins Band](https://en.wikipedia.org/wiki/Rollins_Band) came to be as Henry asked guitar maverick [Chris Haskett](https://en.wikipedia.org/wiki/Chris_Haskett) to join the ranks in creating a new musical endeavor after Henry had left Black Flag. And [Chris](https://www.chrishaskett.com/) is partly to blame for the incredibly musicality that permeates every release from Rollins Band, but one should not forget nor ever underestimate the absolutely incredibly tight rythm-section in this band either. 

Rollins Band sounds like a *stroke-of-the-90s* cynical, post-modern and poetic *alternative rock* band with some really heavy strokes and lots and lots of tastefully executed funk elements. It's inventive, cripplingly self-aware and yet unashamed both lyrically and musically.

[**The Haskett-style 9 string guitar**](https://imagery.zoogletools.com/u/151782/81c6b90567a874dd0b319819f2dcb888814be40e/large/9stringhomepageborder.jpg), a traditional six string guitar, but with the fatter top 3 strings [coursed](https://en.wikipedia.org/wiki/Course_(music)), also paints a unique imagery on all of the records.

{{< youtube vIR-_DzUCJU >}}

If you like an era of 90s bands that appropriated the great sounds formed in the 70s this band is a must and, if you love [**Faith No More**](https://en.wikipedia.org/wiki/Faith_No_More) half as much as me, you need to go through their discography this instant.

> As of writing this, there is a single song by **Rollins Band** on Spotify, which is, at least to me, a great shame and a missed opportunity for people to discover this great band.

Now.. *Should* this band bore you and you're more into weird electronica/beat-music I highly suggest you that you check out Chris Hasketts side-project [**DJ Linux**](https://www.youtube.com/user/ch10014/videos).


#### No.3 NoMeansNo - WRONG

![NMNWRONG](https://img.discogs.com/RZG7efh64ELTpZ6qUgtByq0-F6M=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-988922-1586265618-7719.jpeg.jpg)

**I'm a bass player, and this is the punk band for bass players.** 

[No Means No](https://en.wikipedia.org/wiki/Nomeansno) is a Canadian punk band from Vancouver that really isolates everything that makes punk rock a reputable genre of music in my opinion. 

It's brash, sonically pulverizing but every aspect of the instrumentation is genuinely musical and inventive for its time. 

There's something about popping my *presumably polish bootleg version* of *WRONG* into my cassette player and hear this album kick into gear, starting with the visceral and yet jazzy first track **"It's Catching Up"**. 

I bet one of my more recent obsessions, [**Daughters**](https://en.wikipedia.org/wiki/Daughter), have been taking notes from these guys. 

I mean if you've heard them before, and you listen to *The Tower*... it's right there, from the trance-inducing stuff pummeling to the vertigo-inducing passages.

The bass on this entire release blows me away. 

Every single redeeming quality of punk rock giants preceeding NoMeansNo gets amplified in their music, especially on this record.

While I adore the dynamic nature of **Faith No More**, there is something about the orchestration, production and ferocity in the music that NoMeansNo creates that just grabs me...and makes me want to play bass! 

Get the release with the Bonus Tracks if you can, as it features a cover of *End of the World*, originally performed by [Skeeter Davis](https://en.wikipedia.org/wiki/Skeeter_Davis) in 1962.

---

I think that's it for this one. 

Check these albums out any way you can and get back to me with a comment below telling me what you think! 
