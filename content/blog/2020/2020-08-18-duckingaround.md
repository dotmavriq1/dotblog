+++
author = "dotMavriQ"
title = "Ducking around"
date = "2020-08-18"
description = "A day of good vibrations"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2020-08-18-duckingaround"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-08-18-duckingaround-03.jpg"
alt = ""
stretch = ""
+++

Today is Anders's birthday, **Grattis Anders**!

Anders Wik is an old friend of mine, and if you like to kill some time with entertaining gameplay videos, I highly suggest that you check out [**JASP English**](https://www.youtube.com/channel/UCPtDgJ1nchXGza0xWP8xcXw).

And Anders loves ducks *(and Cheese Doodles... he really loves cheese doodles too)*... little did we know that the day would end up hanging out with ducks.

##### But that's not until later...

Margarida was eagerly awaiting a package in the mail to arrive at Birgers. 

We cleaned the home a little bit and headed off. 

Of course, I had to complicate things because the sender of the package hadn't sent a confirming text message, and so I asked the cashier and she didn't have any clue so we bought coffee for Margarida, a kiwi/strawberry Fanta for me and headed towards the closest Coop.

##### But as we went by Thai Hörnan we decided to stop by. 

They have lots of interesting food from Thailand in particular, and we walked around for a little bit before settling on a *sour mango*. 

I have no idea what a sour mango is or how it differs from a regular one, but the owner suggested that we eat it with sugar, salt, and chili flakes.

It sounds weird but lovely, I want to try it out! (and for 60SEK... it better be good!)

After hitting up Coop to buy some food we went home and killed off some hours watching Twin Peaks.

As I went over my email folder I happened to stumble across an unread email... from the company that had sent us the package.

We... decide that we'll grab it as Birgers, the closest place that accepts packages, closes at `21:00`, and so we'd go after dinner.

We felt a bit uninspired when it came to dinner, but I never say no to Margaridas Portuguese-style mince, and she obliged.

##### And for dessert we tried the Cactus Figs we bought yesterday!

**And here's a quick review:**

Sorry, they suck. 

First and foremost Margarida's hand got assaulted by the buggers as she did her best to peel off the skin, having placed initial cuts into it with a knife as one is supposed to. After we removed all of the splinters from her hand it was time to take the first bite into it... it's ... watery in all the wrong ways... hell even a Kiwano is more interesting... the seeds are BIG and very unpleasant to chew.

**As I told Margarida:** 
If I was stranded on an island I'd eat them, but that's about as much as I'm willing to give them.

We went to Birgers, got the package, wifey's happy and I bought two balls of delicious ice-cream. 

One *Viola/Salt Licorice* and one with *Lingonberry and Fudge* (or *Kola* as we call it in Swedish).

Margarida wanted us to enjoy the good weather so we stopped by the water and sat down on a bench for a little while. 

It was pleasant. 

##### And then the ducks started gathering around us.

Ducks are cute animals. I mean I've heard rumors about their reproductive process and I think I'd rather not know... but other than that they're just cute little birdy animals wandering around, chasing down bread or other, probably nicer things, to eat.

We went home and watched some more Twin Peaks, vibes abound, and then we fell asleep to ocean sounds.

---
##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-18-duckingaround-01.jpg" gallery="gallery" caption="Hellooo little duckies" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-18-duckingaround-02.jpg" gallery="gallery" caption="I wonder which one I should call Anders" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-18-duckingaround-03.jpg" gallery="gallery" caption="Sorry buddies, I didn't bring any bread" >}}
