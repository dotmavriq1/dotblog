+++
author = "dotMavriQ"
title = "The Social Dilemma"
date = "2020-09-22"
description = "Removing a lot of social media services"
tags = ["Life", "Social Media", "Addiction"]
categories = ["Blog"]
aliases = ["2020-09-22-thesocialdilemma"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-09-22-TheSocialDilemma.png"
alt = ""
stretch = ""
+++

Today is a new day, and it's time to finally get rid of all of the social media that has been stealing hours of my productivity every day for half of my life.

I will be going through them one by one and write something about the experience of being without the service, what I most likely will be missing out on and why it's ultimately worth it.

Let's start with the easy ones. 

### DuoLingo

It's not the devils main cohort.

**I have one major personal gripe however:** They serve Portuguese in the brazilian variety and not the European one, simply calling it Portuguese.

*Don't do that for European members. Don't do that at all.*

It has also become a bit more greedy in terms of points-systems and means of getting free members to pay for their service.

It was always a bit too focused on the *points* aspect either way, I never felt like I progresseed as much as the algorithm applauded me for.

Anything like that is fair game in capitalism of course...but I'd rather just read a well written book about a language I want to learn, or watch good videos that teach me a language... or just pay up for MemRise, which I did!

### Imgur 

Imgur is not a horrible service at all.

It is a convenient means of sharing pictures online.

But having an inflated belief in the sustainability of other services hosting your images has often come to the detriment of the end-user.

I've been enlisted in far too many forums were entire chunks, in 10's of percentages of the totality of images on forums and chatrooms just suddenly get replaced with a shutdown-notice or a plea for the end-user to buy premium quickly.

While I think imgur is too smart to succumb to that... a spicy recession, a bad deal or just persistent server failures is all you need to lose images that might mean the world to you.

I got rid of it to be more responsible with the media I produce.

### Instagram 

I used to be really hooked on instagram in all honesty.

Something about the convenience and the early adoption of filters that really did a well needed number on your not-so-impressive photos just held on to me. 

It is also one of the social media platforms where I've experienced the most personal success in terms of likes and metrics.

I do believe I was cruising well over 600 followers at some point...which is arguably impressive for someone that doesn't post selfies where I look available (don't really have the looks for it either, not that I would ever want to.)... and I didn't exactly deliver to a particular clientel either... at least I'm impressed!

I went from being a nerdy kid trying to impress people to adapting genuinely strange behavior like taking pictures of my meals no matter how hungry I was and waiting for certain moments just to capture an experience instead of experiencing it.

The thought was of course that I would be able to share my moment with people around me that follow me and care... but only later did I realize how much it was to the detriment of my experience of life.

People don't need to know that I am somewhat good of a cook unless I can grant them tangible information to accompany the pictures.

That's another thing...The means in which the information is digested.

Sure, you got a like, but people most likely zipped past it and barely even attended to it, it was just sufficiently relatable or impressive for that red hot split second before the scroll on to the next thing that is sufficiently relatable or impressive.

The nail in the coffin for me was when I left bachelordom and I noticed that there was no means of getting rid of the wild plethora of artificially propped up women from my explore feed... I could sit with witnesses, showing 20 minutes of continous deletion but still have the same yield of unwanted content thrown at me.

And it's not like instagram doesn't know what they're doing, they absolutely have enough computer power to free me from dolled up female faces (or worse yet, bodies) with the simple aid of any algorithm... but they won't. 

Instagram thrives on increasing your consumption, coercing you by selling the idea that you are using a service in which you get your warranted 15 minutes of fame time and time again when in reality you're mostly likely valuable because of the amount of product you consume in the pursuit of this.

I don't think I need to finish it off with the countless controversies that Instagram has experienced, such as the ownership debacle or the buy-out by Facebook... 

Not to forget the fact that instead of taking a personal picture that might last you a lifetime, you snap one, crop it to a 1:1 ratio, put the dingle-filter on it and maybe even paste an emoji to cover your embarassed friends face... gee, I bet that'll age well... 

I bet I'll miss out on some of the travel photos my relatives would've wanted me to watch, babyshower pictures and deep aesthetic pictures of rocks with haikus under them... but it doesn't warrant the mind-poison... it doesn't warrant the countless hours you are incentivized to spend on the service that you will never get back.

Instagram, you're headed for the ranch!

### Pinterest

This one was incredibly easy for me. 

I was never their target demographic, and it was obvious to me as I struggled to ever find a proper use for it.
I ended up using my general defence of "retaining the username" by keeping an account, and in order to get anything out of it at all I used it to collect images that invoked nostalgia to me like pictures of computers, toys and whatnot that I reminisce from younger days.

I must've spent less than 5 solid hours on the service throughout the 2+ years I was on it, which is sort of telling.

It's meant to be some sort of *"inspo-collector"* in the same vein as **tumblr** but without the *deviant art*(badumtss...).

If you ever enlarged a single picture of furniture you can expect your feed to be bombarded with it even after you've gone through pains to exclude it from the settings menu. 

I remember when **FunnyJunk** was new on the internet and how much it annoyed me that they watermarked and landlocked *"funny pictures"*. 
You'd be sitting there trying to google yourself to that funny meme you wanted to show a friend only to be redirected to some horrible looking website, and it was impossible to get to the image without the watermark.

Well... Pinterest is FunnyJunk with proper branding, a cookie-cutter clean design and enough crazy users to oversee the fact that unless *ALL* pictures you'd want in your collections already exist on the service, you will have to painstakingly add them manually. And that's no different from any other means of hosting images, of course...

...But if you ever considered using it to host images to show other people, you are forcing this landlock on them and your success rate will never as good as if you were to simply host images via **imgur**, **flickr**, **WordPress** or even **Google Photos**.

It was never worth it.

Pinterest, you're going to the ranch.

### Quora

I'll keep it short.

*It's a trap*, making you huff your own farts more than you'd normally do.

There, *I said it*.

Sorry not sorry.

You will see far more people trying to inflate their own *personal brand* than you'll see tangible valuable information being dispersed in the optimum way.

I feel as if the vast majority of questions ask for peoples subjective opinion rather than people looking for facts too which... is strange in and of itself.

I became slightly aggravated with the service when I was no longer allowed to have my handle, *dotmavriq*, as a slug anymore... sure, it redirected up until the point where I closed my account but it was still annoying.

I do have an aspect of myself that is somewhat of a know-it-all, and there is no need to beat around the bush about that. I think it's quite common and I am in no way unique when it comes to it...and Quora is just another means of tricking people like me to create content for questions that are mostly incredibly low-effort. 

I'd much rather see platforms incentivise people to use the internet better than to sustain a service that helps people reach regurgitated knowledge by people that trick themselves into believing they're sharing things of genuine import.

There are exceptions on there, granted, but the overarching experience, from my point of view, is that they are rare.

...And it forces you to get an account in order to dig through questions, which sort of defeats the universal utility of the service.

Just stick with some good search engines, Wikipedia, or [**StackExchange**](https://stackexchange.com/)... They might be more of a tough crowd, but the formula is there for a reason, ask good questions, get good answers. The only sin commited by StackExchange is that lesser populated topics usually get biased results that aren't necessarily accurate or helpful, as I've experienced with the Law one.

### Reddit

This one warrants its own post seeing as how it was the one social media service I depended on the most and that will affect my habits on the internet the most. 

### Tumblr

I don't even know where to start with this one. 

Most times you can blame any given technology for the shortcomings of a service.

In this case I really do believe that it's the culture.

Tumblr became incredibly popular among insecure people on the internet.

And while we've had weird spots on the internet ever since its inception, Tumblr was the place that kinda made it "cool" to be a person who also thinks that he/she/it is a dragon, brazenly wearing your psychological diagnosis on your sleeve as you eagerly wait for people to *relate*, or in this case, *reblog*.

I've never really had any contentions with the technology as much as the culture and its subsequent effects on things like Code of conduct.

I also never really got into it... I mostly tweaked my website profile in various ways and used it as some sort of meme-repository before my lack of interest kept me off of it.

I guess people will bring up the fact that the ever famous *porn-ban* also brought the end of tumblr... with intended algorithms deleting far more than said pornography... but it was already going south before that.

To me, my single biggest issue came after the buyout by Yahoo and later Mullenweg, forcing people to sign some sort of cookie-waver consent form before even looking at other peoples blogs unless they themselves were registered to the service... landlocking... I see a theme here.

Nope, ranch.

### Twitter

The most jarring aspect of Twitter is that it is well known to structurally reward provocative content.

Because you are meant to express yourself in *140 characters or less* you automatically go for punchlines, slogans and utterances of effect. 

The more compression you apply to a sentiment, the less nuances will exist behind it, but the same degrees of scrutiny will be placed on it nevertheless.

Most people know that Twitter is infected. 

My personal history with Twitter was that I joined when a lot of my high school buddies joined. 

At some point I think about 50+ people from my home town were on there that I knew personally.

I now know of less than two people actively posting that I initially followed from my high school days.

I know one guy from a nearby school that opted for a metropolitan lifestyle of glamour and glitz and followers follow suit... the average user will have a hard time finding people that genuinely care and repost average day to day happenings however.

It's like an RSS feed to follow fridge magnet quotes, anecdotes and critiques from famous people these days.

And no one really needs that, I certainly don't.

I will finish off with [**How one tweet can ruin your life | Jon Ronson**](https://invidious.snopyta.org/watch?v=wAIP6fI0NAI) that also exists in long-form, known as a book called [**So You've Been Publicly Shamed**](https://duckduckgo.com/?t=ffab&q=so+you%27ve+been+publicaly+shamed&ia=web).

...Ranch!

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-09-22-TheSocialDilemma.png" gallery="gallery" caption="Art for the article ;ppPPpPpPpP" >}}
