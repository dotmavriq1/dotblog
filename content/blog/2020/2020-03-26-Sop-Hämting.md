+++
author = "dotMavriQ"
title = "Sop Hämting"
date = "2020-03-26"
description = "Kojima-san, art and prophecy"
tags = ["Life", "Corona", "Death Stranding", "Hot Takes"]
categories = ["Blog"]
aliases = ["2020-03-26-sop-hamting"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-03-26-sophamting.jpg"
alt = ""
stretch = ""
+++

##### Am Porter Bridges

I was asked to remove cartons that were clogging up the entrance at work this morning. 

Looking at all of it I realized it would be somewhat of a logistical hardship to do in one go. 
Not that any of it was even remotely heavy but rather that the larger cartons contained so much junk that I would have to carry all of it in the boxes that contained the most stuff... and it was heavy-handed work as it turns out.

After moving all of it out of the office I had to fit it into the giant elevator, remove them from the elevator on the lower floor and later repackage and minimize space so as to lessen the amount of times I would have to walk to the garbage collection facility. 

[**In the times of corona**](https://open.spotify.com/track/5AJrhrwz4oSZX2PwwV4qrN) I had my travel-disinfectant ready at hand... sitting on the floor next to the lower level of the elevator, crunching carton... I had two people walk by and the classic awkward Swedish situations ensued where they stood there, waiting for the elevator to come...but no small talk. 

Especially not during these times. 

I may be a bit neurotic, but I can somewhat sense that it has already started affecting peoples attitudes.

While carrying the cartons from my then-current spot to the garbage collection facility located fairly close-by I decided to listen to the soundtrack of Death Stranding.

**Death Stranding**, as I've mentioned in previous posts, is about a post-apocalyptic world. You have the looming threat of the **BT**, which is this 'Negative Zone' other-dimensional dark thing, kind of playing into the symbology of zombies, radiation... but also infection! 

In this world you play as Sam Porter Bridges, a man destined to unite a post-apocalyptic America by being the **heroic glorified pizza-delivery dude**. 

This has killed the hype for some players that probably enjoy other Kojima titles such as the Metal Gear Solid series while others have ended up enjoying it for what it was. I belong to the latter category, for sure.

I *did* spend parts of my Christmas playing it while staying with Margarida in Portugal. You can read more about it here if you haven't. I didn't get to finish it though, obviously, but look forward to doing so. 

But I didn't want to write more about Death Stranding as much as I would want to point out my reflections as I spent my morning the way that I did.

*Time for a Hot Take*

##### The world of Death Stranding already reflects many aspects of a contemporary corona-ridden world

Think about it.

The looming threat of global warming and pandemic disease will force the job market to adapt to a more remote work model in order to be economically sustainable.

We are already seeing political movements, especially in Europe but you can also see reflections of it in the rust-belt areas of America where people who have decided to live a life as most westerners wish to live with moderate seclusion, a nice and safe neighborhood, maybe some land worth mentioning for the kids to play in et.c. ... They are finding it increasingly harder to sustain a life where you travel to work with gas prices going up.

And the incentives to move the focus from metropolitan cities are virtually non-existent. The management class, or the [*Anywheres*](https://www.theguardian.com/books/2017/mar/22/the-road-to-somewhere-david-goodhart-populist-revolt-future-politics) as British Journalist David Goodhart calls them, want to remain in the big cities because it's the necessary environment for them to climb their given hierarchy. They think that they have every monetary and social motivation to stay inside of the big cities. 

And I understand that it's a logical conclusion to draw. 
It's the path of least resistance for that group of people. Why would you risk squandering a career in order to theoretically appease a *debonaire workforce* in the middle of nowhere, right?  

But the environmental toll as well as the potential health risk posed in a not-too-distant future may well prove to spell the end of this.

We will have to move to remote work with the aid of the internet as well as create spaces that are more secluded where coworkers can congregate whenever necessary outside of the big cities. 

Professor in Economic History, Lars Magnusson, worries about the future of smaller cities in the now post-industrial society we live in.

Needless to say we really do need to think fast about sustainable infrastructural improvements in light of these challenges.

I listened to [Sam Harris speaking to Matt Mullenweg of WordPress fame about the future of work](https://samharris.org/podcasts/194-new-future-work/) as well as [Luke Smith and his unaboomer-rant about roads](https://invidio.us/watch?v=geBQNOid_7A). They play well as additional listening if you're not sufficiently satiated for the day.

Beyond the geopolitical effects, the obvious Death Stranding moments occur as I observe more and more companies entering the delivery service in order to sustain.


I just that saw [Rotana](https://www.openstreetmap.org/?mlat=58.50705&mlon=13.15752#map=18/58.50705/13.15752&layers=H), the local pizzeria/kebab shack located close to work are now offering free delivery anywhere in the municipality, and they're far from the only ones doing this!

Time will tell if it ends up being economically sustainable for most restaurants... who knows, maybe even convenience stores will start delivering goods in order to sustain.

We may end up living in a world where everyone is a glorified pizza delivery hero, I'm just saying.

Stay safe everybody.
