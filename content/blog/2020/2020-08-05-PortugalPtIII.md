+++
author = "dotMavriQ"
title = "Portugal Part Three"
date = "2020-08-05"
description = "Faintly we go. "
tags = ["Life", "Corona", "Travel", "Portugal"]
categories = ["Blog"]
aliases = ["2020-08-05-PortugalPtIII"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Portugal3/Portugal3_06.jpg"
alt = ""
stretch = ""
+++

I wake up at `05:30`, minutes before the alarm starts beeping.

Bags are packed, Margarida starts getting ready while I adjust to waking up.

I cut my beard, freshen up and we start heading out. 

We're out the door at around `06:00`, which gives us roughly 12 minutes before the train departs, so we're getting a move on.

This is where we sadly miss to bring the Västerbottenost and the Rhubarb that we were going to bring to Portugal, *huge bummer*.

#### Oh well. 

I haven't traveled since Corona started, and while I feel calmer than ever in terms of the logistics of the travel it was an interesting experience to see how people act in times of viral crisis. 

We decided to take the train that doesn't have any transfers and that leaves us with enough time to get ready for the flight and everything.

On the train from **Lidköping** to **Gothenburg**, *there was not a single person wearing a mask*. 

It was a calm, beautiful train ride, *as always*... it's nice to get a good view of the Swedish countryside.

We arrive at the least populated **Gothenburg Central Station** I've ever experienced and walk to the **Nils Ericsson terminal**. 

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_01.jpg" gallery="gallery" caption="The airplane bus had this advertisement for Mullvad VPN. Classic Swedish VPN company" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_02.jpg" gallery="gallery" caption="Me having to get used to wearing a mask" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_03.jpg" gallery="gallery" caption="Obligatory couple-selfie with masks" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_04.jpg" gallery="gallery" caption="Off we go to Frankfurt" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_05.jpg" gallery="gallery" caption="We avoided the first toilets we saw, as they seemed crowded, Margarida went into the second womens bathroom she saw...the adjacent one, for males, was out of service..." >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_06.jpg" gallery="gallery" caption="Frankfurt" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_07.jpg" gallery="gallery" caption="Before landing in Portugal, we got a notice of what to do if we get infected with COVID-19" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_09.jpg" gallery="gallery" caption="Outside of the Lisboa airport]" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="Portugal3/Portugal3_10.jpg" gallery="gallery" caption="Finally in Margaridas bed in Portugal, with an ice cold Mini in my hand" >}}
