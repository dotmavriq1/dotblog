+++
author = "dotMavriQ"
title = "Rain Walk With Me"
date = "2020-08-21"
description = "More Lynch, lots of rain and great food"
tags = ["Life", "Twin Peaks", "Philosophy"]
categories = ["Blog"]
aliases = ["2020-08-21-Rainwalkwithme"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-08-21-rainwalkwithme.jpg"
alt = ""
stretch = ""
+++

I pop out of bed and fix Margui some coffee, we proceed to watch some Twin Peaks.

Yesterday we finished the second season and so we thought it was time to watch the movie, **Fire Walk With Me**.

I can understand why some critics don't like it but in the grander scheme of things, in all the ways it elaborates on the universe that Lynch creates in Twin Peaks, I enjoyed it.

##### It closed in on lunch time 

And as we were hungry we went with an early lunch. 
Margui is fasting so I decided to have som pytt-i-panna just to get it out of the way.

**Missing Pieces** is a collection of deleted scenes from the *Fire Walk With Me-Movie*... It went slow as molasses. 

I think we may have exhausted ourselves on Twin Peaks for the time being.

We finish watching it, finally, get bored enough to go to a big Coop, I wanted to see if I could find a tool for draining the pipes under the bathroom sink, as the water is slow as molasses. I've never drained a pipe with the aid of a tool, but I enjoy firsts so...that might be exciting... or just plain disgusting...We'll see!

I check the mailbox on the way out and I finally see that my book has arrived, *René Guénon*'s **The Crisis of The Modern World**. On the back it reads:

> It is no longer news that the Western world is in a crisis, a crisis that has spread far beyond its point of origin and become global in nature. In 1927, René Guénon responded to this crisis with the closest thing he ever wrote to a manifesto and 'call-to-action'. The Crisis of the Modern World was his most direct and complete application of traditional metaphysical principles-particularly that of the 'age of darkness' preceding the end of the present world-to social criticism, surpassed only by The Reign of Quantity and the Signs of the Times, his magnum opus. In the present work Guénon ruthlessly exposes the 'Western deviation': its loss of tradition, its exaltation of action over knowledge, its rampant individualism and general social chaos. His response to these conditions was not 'activist', however, but purely intellectual, envisioning the coming together of Western intellectual leaders capable under favorable circumstances of returning the West to its traditional roots, most likely via the Catholic Church, or, under less favorable ones, of at least preserving the 'seeds' of Tradition for the time to come.

It was recommended to me by a friend, I intend to read it, *take notes* and probably write about the book here on the blog when I'm finished.

I haven't exactly delved into '*Traditionalism with a big T*'-readings before... I know Margarida has read some Evola and wasn't a fan... but there are some scholars that think that René is several leagues beyond what Evola could ever aspire, and while Evola is loved by short-sighted fascists in the scholars eyes, René is far more poignant in his analysis and correlational abilities when it comes to perennial truths and spirituality in both West and East. 

[I did enjoy the **Sweeny vs Bard-podcast episode** that picks up on topics surrounding it, but I don't really feel as if I got a lot of insight into Guénon or his stances.](https://poddtoppen.se/podcast/1470131813/sweeny-vs-bard/sweeny-vs-bard-season-2-ep-16-steven-bannon-and-the-traditionalists-with-benjamin-r-teitelbaum)

##### Where were we? Oh right... we're on our way out the door

As we go out we sense a little rain coming on, Margarida takes her new portable umbrella out of her purse.

We walk past the school and by the time we pass Örthagsgrillen it starts to rain so badly that we stay a brief moment under DinX.

##### I say we keep on as the rain won't give any time soon...

We have another brief pause from the rain in the bus station near Mariagården only to finally end up near Jysk...

We start by looking for bedsheets but ultimately decide, as many times before unfortunately, that we need measurements before we purchase any bedsheets.

We swing by Elgiganten but find nothing and head over to Överskottsbolaget.

**We don't know what to look for**. 

Margarida would like a quaint little notebook or maybe a dedicated diary.
I buy an oven form fit for two people to make pies in the future. I also buy rectangular post-it notes for note-keeping while I read my new book.
Margarida can't find a diary so I look for some kind of draining tool as I said.

This is where we randomly meet the one and only Damien Terry... I think I may have seen some tall figure earlier, walking around in the store, but I tend to focus on what I'm about to get and so suddenly he was in front of me hahaha, nice to see him as always though.

Damien hosts a Swedish speaking Metal Podcast, I'd say it's the best Metal Podcast in Swedish there is. It's called [**UPPGRÄVT**](https://poddtoppen.se/podcast/1493817330/uppgravt-en-svensk-metalpodd) and if you happen to know Swedish and you happen to like metal you better be listening to this podcast, that's all I'm saying.

##### Anyways...we say goodbye to Damien and leave the store

We head to Clas Ohlson and to my surprise they had a pipe-draining tool.

We go to Coop, buy some dinner, leave and the weather is ambiguous, we decide to swing by LiDL while we're still outside.

We found chanterelles, some nice shallots, nuts and stuff we like... and on the far end of the story, I see a face I recognize.

It's Tony! I haven't spoken to that guy since arguably the Makerspace-days... 
It was nice to meet him and just shoot the shit for a little bit, but I couldn't bother him for too long of course, as he was on his shift.

Margarida also ended up surprising me by finding me those cherry fruit bars I enjoyed.

##### We go home, cook, and prepare ourselves mentally to start season three of Twin Peaks.

I've heard that it is going to be a different experience.

But first, I decided that I'm done feeling uninspired with my cooking, and so I make a three-course for me and Margarida on this lovely Friday, despite the unfortunate weather. 

We watch some [Life and Death Row](https://en.wikipedia.org/wiki/Life_and_Death_Row) while I cook, this documentary series that the BBC made regarding Death Row legislations in America... not sure why we went with that after all of the weird Lynch-stuff... but it served as some sort of *pallette-cleanser* while I made all the food.

* As a starter, I wrapped prosciutto around asparagus and treated them with olive oil, spices, and some lemon zest. 

* I then marinated a chicken with lots of lemon and spices, a chicken that also went into the oven, basted several times until the skin looked lovely and the meat was tender enough. This was served with parsnips, which has been Margaridas craze for a good while now... something I don't mind I mean... Parsnips are awesome! Great main course!

* For dessert we both realized we had that sour mango and so I cut it up... I'm not sure it was quite ripe enough as it has a strong *"stem"* inside of it... but the meat around it was very soft, juicy and lovely. I served it with a bed of muscovado sugar, sea salt, and chili flakes and to the surprise of both me and Margarida we loved the fruit and the spice blend worked well! Thank you Mr.Thai Hörnan! 

##### We start watching season three of Twin Peaks. 

My initial reactions are that Lynch keeps on co-opting the modern medium and betraying his audience in a good way, just like Kojima and my other favorite creative people. A lot of old actors are there, which is great, both I and Margarida kinda missed Donna in Fire Walk With Me and stuff so we were worried it was going to be an ongoing trend. Pleasantly surprised so far.

The day kinda ended on a strange note though... I wrote about those cherry bars that Margarida got me right? 
Well... I unwrapped one while we were watching Twin Peaks, not exactly looking at it... it kinda tasted like bad wine... 
And under the light of a brighter scene, I see from the reflection of the television screen that I had just taken a good bite, and subsequent swallow, of a moldy cherry bar... **foda-se**...

Here's to possibly spending the night puking and missing out on tomorrow's adventures... *just my luck*...

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-21-rainwalkwithme-04.jpg" gallery="gallery" caption="Asparagus with Proscuitto" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-21-rainwalkwithme-03.jpg" gallery="gallery" caption="Asparagus with Proscuitto...CLOSER!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-21-rainwalkwithme-05.jpg" gallery="gallery" caption="The Lemon Chicken before it finishes in the oven" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-21-rainwalkwithme-02.jpg" gallery="gallery" caption="Lemony Chicken goodness!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-21-rainwalkwithme-01.jpg" gallery="gallery" caption="Sour Mango with spices" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-21-rainwalkwithme.jpg" gallery="gallery" caption="Art made for this article" >}}
