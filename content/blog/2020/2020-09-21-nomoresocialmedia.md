+++
author = "dotMavriQ"
title = "No more social media"
date = "2020-09-21"
description = "My goodbye to social media"
tags = ["Life", "Social Media", "Addiction"]
categories = ["Blog"]
aliases = ["2020-09-21-Nomoresocialmedia"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-09-21-nomoresocialmedia.png"
alt = ""
stretch = ""
+++

It's hard to know how to start a post like this...feels like I set myself up with a title like that.

But it does sort of mark where I'm at emotionally. 

For most of my life, I've always felt as if I'm underachieving. 

It might be tied to issues relating to self-worth or maybe something else that I haven't exactly figured out.

In most aspects of life I have learned how to cope with my shortcomings and improve with time.

I'm somewhat street-smart and I have been a bit dull at times when it comes to life choices but I'm certainly no *butterknife in the proverbial drawer*.

Closing in on the age of 30, I often ponder on what I could have achieved if only I took time out to commit to things instead of procrastinating and this has, in various degrees, continued to plague me for most of my adult life. 

And while I think that the core of the problem, all things considered, certainly has to do with me and my shortcomings and that I can find means of escaping responsibilities by justifying my procrastination, there are things that I could do to significantly improve my success rates.

I won't use this particular post to convince anyone else to do what I am about to do.

**I am doing it for me**.

But I'd be lying if I didn't tell you that the climax of my worries wasn't eclipsed by viewing the documentary **The Social Dilemma**. 

It's no *Sundance high art achievement* of a documentary, and personally, it did not tell me a single thing that I did not already know.

#### *But it did frame it for me.*

Social Media has gone from primarily being a hosted platform for humans to engage in human behaviors, be they good or bad, to becoming data farms of unprecedented scale, utilizing algorithms with peak scientific and economic incentives to get you to stay for as long as possible, to engage as much as possible and to click advertisements as often as possible. And the social effects and consequences of contemporary social media do not stop there.

We already have hard facts. 

We know it makes us less happy, makes our attention spans worse, plays key parts in cultural divides and there's no real way out of it as far as the experts can tell. 

...and I'm at a point in life where I have everything I need and more to live a rich and fulfilling life.

I have my trajectory set. I have a woman that I have been in a loving relationship with for over four years now. 

I have a family that I love and that loves me and supports me. 

I have friends that are far better than I could ever truly deserve that believe in me and my capacity to do great things for myself as well as with them.

I know what I want to become. 

And I know what is stopping me.

Social Media is playing a big part.

And so it's time for me to leave.

I want to do this not as a purely ideologically motivated protest or anything with that degree of grandeur.

I am doing it to see how much time it allows me to put into my other core interests, whose priority drills into my mind every waking moment.

I want to learn Portuguese so that I can understand more of the culture of the woman I love, for the sake of connection, of course, but mostly for me.

I want to take my health, mental as physical, more seriously, this means spending more time working out as well as giving meditation a real run for its money over an extended period to see if I can find it to be of real benefit.

I want to attempt to reach Full Stack comprehension. This means learning entire programming languages, as well as updating the basic knowledge I already have, understanding stacks, and keeping track of current developments. I enjoy coding and there are money and a future in it for me as long as I take the time out to learn it.

I want to play my instruments and spend time with my studio equipment for the sake of me and the promises I've placed with the members of my band.

And lastly... I want to do what I am doing right now. 

I don't have the best memory in the world, and there's always something striking that happens to me whenever I truly reminisce and reflect on what I've done and the progress I've made. It makes a huge difference for me, if even only for me. So I will continue to keep this blog updated, for me, and you're welcome to follow my developments if you want to.

---

### Here's what I will do:

**Start by deleting unnecessary Social Media services:** 

These include services that I've only really kept with the excuse that I'll regret not taking the *" username space*", only to visit once a month or less, always with a feeling of dissatisfaction with the services. Services like **Tumblr** and offshoot social media accounts that I've mainly just tried out only to never adopt because I never understood their utility, like **Pinterest**. I will keep **LinkedIn** as it is impossible to abuse for me... quite frankly it's a boring service, and it'll make it easier for me to look for jobs abroad in the future.

**Delete the ones that are controlling my life:**

The absolute toughest one for me will be **Reddit**, as it is something I have been habitually checking for the past six years. However, there is no doubt in my mind that the one with the most severe social consequences will be **Facebook**. It will lead to inevitable questions regarding my health, my relationships, residential status et.c only to then turn into structural neglect, something I have previously experienced during a breakup with my most recent ex. I cut social media to focus on myself in times of confusion only to find the harsh truths of structural neglect... people have Facebook as a convention now and are thus far more likely to not be the one breaking from the herd to reach out to you whenever there's a party being arranged et.c, humans just work like that. Facebook has you trapped with social convention, but I feel way more prepared to deal with this than I was back then. I talk to everyone that matters to me, and if cutting Facebook stops conversations from being had, they must not have been that important for either people involved, to begin with. **Twitter** has become more popular for me when it comes to following the social deterioration that is out there... that and the occasional shoutout from people I admire... I know I don't need it. I've stopped with **Instagram** altogether for years, but I still have the account, it's time to get rid of it if anyone does care about my mundane life activities...well...They have the dotBlog.

**Find replacements for tracker-sites:**

They are not traditional social media sites, but I have placed value in services that allow me to easily track books I want to read, movies I want to watch et.c
I do have some slight ethical contentions with giving all of this data away to companies, however, and as an extra piece of motivation to get me to code more, I guess I'll have to code some of my own to create my own trackers.

**To infinity...And beyond:**

I want to be able to replace **Skype**. It's a proprietary blob that has only really stuck with me because no other service does what it is capable of in terms of delivering consistently stable internet phone calls overseas. I'm not addicted to the service as much as I'm stuck with it as no other alternative has been found viable throughout the four years I've been in love with Margarida... but *skam den som ger sig*, as we say in Sweden.

---

#### So how do I get a hold of you now? 
If you are a relative or a friend that wants to keep in touch I suggest that you start by [**emailing me**](Jonatan@dotmavriq.life) in case you don't already have my phone number... either you do that or you ask someone that has it... I bet I'll be happy to see a text or an email from you, no matter who you are!

If you are a nerdy, fast typing chatroom kind of person I do have a decentralized chat server running the ever-awesome Matrix protocol (It looks and feels kinda like Slack, Discord, or Mattermost). Send me an email and tell me you're interested and I'll be sure to hook you up.

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-09-21-nomoresocialmedia.png" gallery="gallery" caption="Inside Gudhems Kyrka" >}}
