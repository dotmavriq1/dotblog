+++
author = "dotMavriQ"
title = "Bug Fables"
date = "2020-11-20"
description = "Approved by fans of Paper Mario? Count me in!"
tags = ["Rant", "Games"]
categories = ["Blog"]
aliases = ["2020-11-20-BugFables"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-11-20-BugFables1.jpg"
alt = ""
stretch = ""
+++

[I recently wrote a lengthy "impressions-style" blog post about how I perceive the newer installations in the Paper Mario franchise and how disappointed I am in the direction they are taking.](https://blog.dotmavriq.life/blog/2020/2020-11-14-lookathowtheymassacredmypapermario-copy/)

While researching the topic I checked out [Metacritic reviews](https://www.metacritic.com/game/switch/paper-mario-the-origami-king/user-reviews?dist=negative) for the latest installation of Paper Mario, *The Origami King*. 

There's also a good review on [HeyPoorPlayer.com](https://www.heypoorplayer.com/2020/09/01/paper-mario-the-origami-king-review-switch/) which is worth checking out if you're out of the loop, with poignant critiques like the passage that follows:

> And yet, once again, it breaks my heart with a half-baked combat system. As someone who believes Paper Mario: Sticker Star might very well be the most offensive product released in Nintendo’s long history — a tortuous anti-player exercise that’s a fundamental betrayal of the core accessibility pulsing not merely in Paper Mario, but in the very heart of its physics-based forefathers that continue captivating the world — believe me when I say I do want to love Origami King. There’s a real heart and passion to this one which my Nintendo-captivated heart knows rises to the series’ former glory. Maybe I can’t let go of a grudge. Maybe I’m still burnt from Color Splash‘s honeymoon period, where not even a gut-busting script could mask the rust innate within the ill-conceived “card” system. But deep in my vellum-weary soul, I know exactly where the problem lies. Once again, in the series’ misguided rush for innovation, the latest Paper Mario commits itself to an identity crisis: one where its overlapping genres tie an uneven bow on dubious gameplay concepts.

While some of the reviews on Metacritic and elsewhere were positive, stating that it's a playful game with a lovely aesthetic, great theme, and innovative gameplay, there were some, of which I would assume myself to belong to, stating that it was a disappointment and a sham for the continuation of the first two titles, as every subsequent release has been.

Not only did they say this though... there was a substantial amount of them stating that *"it's better to just buy Bug Fables on Steam"*. 

I think I had heard about it in passing but never paid it any mind...but now, with the context given... I decided to read up on the game... and as it was on sale with a decent discount I decided to purchase it as well.

**I have now enjoyed a good 10+ hours of gameplay and here are my first impressions:**

I love it!

I absolutely love it! 

I thought the story would be too alienating for me, as what I would want would be Paper Mario and that entire universe... but it only takes a couple of rows of dialog to understand that you are in good hands in terms of story, writing, pacing, humor and all that you would require from Paper Mario.

The gameplay mechanics are flexible, trying but enjoyable, and original but not in a way that makes it foreign to anyone that was an avid fan of the first two Paper Mario games.

There is SO much to do as well! 

I played it together with Margarida, as she is staying here this week... and we both reminisce our great memory of playing *Mother 3* together and how much fun we have when we play this cozy type of RPG together... 

If any contributor to **Bug Fables** gets the chance to read this: Thank you, from the bottom of my currently thawing heart! This is all I wanted and more! 

**And to anyone that owns a Nintendo Switch:** Please, for the love of Venus, buy Bug Fables, it *is* worth it... and it will hopefully show Nintendo how wrong they have been with the direction they've taken Paper Mario.

I'll be sure to post a review when I've played the entire game.

**Until next post:** 

[It's atrocious and disgusting. Perfect for my bookshelf. Here's your reward!](https://old.reddit.com/r/BugFablesMemes)

---

[I occasionally post my Paper Mario gameplay on my YouTube-channel, here's a link so that you can view my channel through Invidious.](https://invidious.snopyta.org/channel/UCa0xHQDkmyHUg5gRGLc8JTw)

---

# Fin.

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-11-20-BugFables1.jpg" gallery="gallery" caption="Gameplay screenshot of me playing Bug Fables for the first time!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-11-20-BugFables2.jpg" gallery="gallery" caption="I love this game so much already!" >}}
