+++
author = "dotMavriQ"
title = "Bitterljuvt"
date = "2020-09-13"
description = "Another day at the grind"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2020-09-13-bitterljuvt"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-08-22-TwilightOfTheCheeseGods-15.jpg"
alt = ""
stretch = ""
+++
So it's monday yet again.

I could probably use a week of sleep to recoup but such is life, back to work it is. 

I think it might be whatever I'm allergic too that is bugging me again, because my eyes are itchy once again and all that.

Getting pictures of Portugal from Functor added to my already immense saudade. 

Mondays work was mondays work.

I did some sorting in a WordPress database, nothing too enticing... But at least I feel like I found ways to work faster so that I can be done with it quicker. 

Had a bit of a late lunch consisting of two chouriço and cheese toasts, couldn't be arsed to have anything more fun than that... But at least it wasn't pytt-i-panna, so that's a step in the right direction I guess. 

One good thing is that I have sort of rejuvinated my love for quirky hardcore and thrash metal, so I made a playlist.

Speaking of which, I've veen thinking of how I could share playlists without strictly forcing people to use Spotify, and I will talk about that at greater length some other time... But it did get me to check out Archive.org further.

I'll try to use it more frequently, as I believe in the cause.

I went for a walk for my health and to help with sleep, ended up buying some food for tomorrow as well before Coop closed.

Rest of the day was spent doing some braindead activities. I played two hours of Wasteland 3.
I think I'm in love with this game. The story keeps getting better and I feel like the difficulty is much more balanced than in the second installation, where I would find myself softlocked and stuff. 

I talked to Margarida, it was nice as always but I'm so tired at night, yet unable to fall asleep. Förlåt älskling. 

At least I did my Portuguese today, and it felt really good, hearing that Margarida might be able to show up here in November was a great morale boost. 

I'll finish the night watching some Spider-Man: The Animated Series, a childhood favorite, easy watching to unwind the mind and then I'll fall asleep in a freshly made bed.
