+++
author = "dotMavriQ"
title = "There are many here Among Us"
date = "2020-12-08"
description = "Who think that life is but a joke"
tags = ["Life", "Corona", "housekeeping"]
categories = ["Blog"]
aliases = ["2020-12-07-TherearemanyhereAmongUs"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-11-08-ManyHereAmongUs.jpg"
alt = ""
stretch = ""
+++

I thought I could turn this day into a productive one... but wasted a good couple of hours on simply switching off my alarms for weekend days.

So.. I, of course, woke up at 12:15... *jesus* Jonatan... 

I immediately started working more on the blog... I have just about added more than half of all of the old entries for 2012 that has been a long way in the making.

There must seriously be over 2000 blog posts waiting to be properly published for this blog...but at least today I put a dent in it.

While I'm coding away and editing these posts I noticed that my dishwasher started making sounds I've never heard before.

It finally reached a point where it started blinking on the rightmost mode-setting...which must be some sort of error code.

I checked and there's milky, unclean water in the bottom of it...even though all of my cutlery and plates and stuff have come out clean.

*Ooook...* So I started by removing the salt filter...which I've never touched before... mock me all you want but I actually had no idea that it even existed in the first place, haha! 

Eehrm... it smelled a little bit...so I cleaned it out thoroughly...put some cleaner in the designated space and ran the water program roughly 2-3 times... one time there was a steady stream of water from the tube that goes to the sink which to me indicates that it is *not* suffering any significant blockage...that can be both a good and a bad thing, I guess.

I removed the filterpiece and it was almost entirely clean... so... maybe it's some piece of internal electronics or a mechanical piece that's broken down... I'm not quite sure yet.

I'm buying some dishwashing salt in the hopes that it will sort itself out.

Meanwhile I've been doing two faster shower programs and a thorough cleaning program(or whatever program it is..the one with the clock on it)

and it seems like it may have fixed itself... I will go through the filters and stuff AND give it a run with salt tomorrow.

---

##### Video for today:


{{< youtube fzlyPIB1sI8 >}}

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-11-08-ManyHereAmongUs.jpg" gallery="gallery" caption="Art for this post" >}}
