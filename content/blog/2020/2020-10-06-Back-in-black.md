+++
author = "dotMavriQ"
title = "Back in black"
date = "2020-10-06"
description = "An eventful and good day"
tags = ["Life", "Corona", "Duva"]
categories = ["Blog"]
aliases = ["2020-10-06-Back-in-black"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-10-06-2.jpg"
alt = ""
stretch = ""
+++
With the morning meeting set at 09:00, I struggled on low sleep for yet another morning and headed to the office.

It was really nice meeting everybody there.

Corona has sort of aided in setting bad habits I guess, what was once genuine precautions are now replaced with the subtle feeling that it's *simply for the better* to just stay away from people... but it most certainly isn't, as the doctor pointed out the other day.

Anyhow, the morning meeting was moved and I went about my day, starting by sitting in the sofa-space of the office until I finally moved to a place where I could dock my computer and use all screens to maximize productivity. I did some category migrations for a database I've been maintaining for quite a while but I felt like I had a fresh start at approaching it this morning and things went well all throughout.

My boss has made the office look really neat, maybe I'll post a picture of it when it's all done for full effect.

I got to decide lunch as I was planning on meeting *Functorism*, so we decided to swing by **Lidköpings pizzeria**, my favorite conventional Swedish pizzeria in town.

I went for a *Paradiset* which is a goat cheese, honey, ruccola, and walnut pizza. *Yummy*!

Phil drove me, David, and Niklas over there and we had a fun ride there as I got to troll everyone with a honking horn that Phil had laying around in his car, good times.

Got my pizza, we went back, *Functorism* was a bit under the weather at that moment and so we decided to meet up later.

Before my lunch break was over I took some time to configure `arandr` to execute properly on my work computer. I had it set up properly back when I was running Arch on it but now for *Manjaro i3 Community Edition* I needed to apply some slight tweaks to get it to play nice with multiple monitors. 

I also tried the `i3lock-fancy-dual monitors` package from the AUR and it played nice... might load a little slowly but it pulls off what I want and so I think I'll spend some time trying to get my Manjaro-setup neater on my work-computer whenever I have time over.

Kept working for a little bit, good vibes in the office, got stuff done.

Eventually *Functorism* showed up and we talked about code. It's always inspiring to discuss code with that guy as he's amazingly talented and passionate about programming. He handed me these really cool small prints of **The dotblog**, printed with a receipt-printer, they're really cool! 

As the day reached its end I was reminded by Toby that we had planned on making dinner together.

We just went complete bonkers with the choice of food and went for *Polenta with moose mince*.

We sourced everything, went over to my place, cooked it and I'd say we enjoyed it.

There are things I would've done differently in order to nail a dish that attempts the complexity we added to it, and the polenta, even though cooked as if from a recipe, did end up with a weird slightly burnt aftertaste that stole the show a little bit too much for my liking all things considered... But it was fun, and that's ultimately what matters.

Toby was really kind and offered his advice as I further my ambition to build a new desktop build with the *Coolermaster HAF XB EVO*. 

And I also checked my mail today and both my t-shirt orders have arrived, so now I'm a proud owner of a [**Viagra Boys**](https://en.wikipedia.org/wiki/Viagra_Boys) t-shirt as well as a [**JASP Gamer**](https://www.youtube.com/user/JASPGMR) t-shirt!

I emailed [Inet](https://inet.se) as well as [Komplett](https://komplett.se) and asked them for suggestions too and, in conjunction with advice from all of my other friends I have high hopes I'll be sitting on a crazy good build real soon.

Mikko, the friendly neighborhood sound engineer, sent out the tracks from the recording sessions I did a few weeks back so that I can finally create practice-tracks for me and the band, especially for me and Adrian as we learn the songs more properly.

My day was finalized with a really good conversation about everything and nothing with Margarida.

I look forward to heading back to the office tomorrow... but first I need to get some good hours of sleep, it's time to break out of the crap-cycle I've been in so I can return to my commitments.

Hold on people, and stay tuned!


##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-10-06-2.jpg" gallery="gallery" caption="Glad to be back at the office. And there's the prints I got from Functorism!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-10-06-1.jpg" gallery="gallery" caption="The Pizza by the name of Paradiset, ordered at Lidköpings Pizzeria." >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-10-06-3.jpg" gallery="gallery" caption="I got a clown-nose too, so here's the ever-important selfie of the occasion" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-10-06-4.jpg" gallery="gallery" caption="The shirts I got today!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-10-06-5.jpg" gallery="gallery" caption="The polenta" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-10-06-6.jpg" gallery="gallery" caption="Topped the polenta with the moose mince, some habanero and some diced tomato" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-10-06-7.jpg" gallery="gallery" caption="Some 18 months aged cheddar, The Ädelost Special 45% and Lingonberries...because why not?" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-10-06-8.jpg" gallery="gallery" caption="Voila, what a beautiful abomination!" >}}
