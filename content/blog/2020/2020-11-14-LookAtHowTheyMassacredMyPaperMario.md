+++
author = "dotMavriQ"
title = "Look how they massacred my Paper Mario"
date = "2020-11-14"
description = "The decline of a beloved RPG"
tags = ["Rant", "Games", "LHTMM"]
categories = ["Blog"]
aliases = ["2020-11-14-LookAtHowTheyMassacredMyPaperMario"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-11-14-MassacredMYPaperMario.png"
alt = ""
stretch = ""
+++

I love Paper Mario. 

I mean I really really love Paper Mario.

When I grew up I had the pleasure of inheriting a Nintendo Entertainment System with more than 30 games and lots of controllers and extra gear, and that was my introduction to not only Nintendo games but to console gaming as well. 

I got my ass handed to me with a lot of the titles I had for the *NES* and I know that I owned a cartridge of *Faxanadu*, but I never really got that far in it, nor did I understand the mechanics as a young child...

And then I later got a Gameboy Color, enjoying my very first portable experience with games (unless, of course, you count Snake on the *Nokia 3310*).

And on the GameBoy, I think I played my very first RPG... If you can call PokeMon Blue an RPG, that is.

But when the Nintendo 64 came along I was fated to plague my parents until I could have my hands on one.

Seeing Mario run around in three dimensions was easily the most amazing thing my young eyes had seen at the time.

I think I was in a Mario-themed room in IKEA the first time I saw it actually, hah!

And then I got one...and it's not until years later that I've reflected on the fact that I only ever owned three titles for my N64.

Super Mario 64, Super Smash Bros, and Paper Mario.

I got mine a bit later than all of my friends, some games required some crummy memory-adapter that was expensive for someone with an allowance... And I played a lot of titles with friends in their homes instead, like Ocarina of Time, Donkey Kong, Diddy Kong Racing, and GoldenEye, among others.

So yeah, there were factors at play for why I only ever got three games for the N64.

But I know the main reason.

The main reason is the last game I got one fated Christmas.

It was Paper Mario.

> Set in the Mushroom Kingdom, the game begins when Mario and Luigi receive an invitation from Peach for a party at the castle. There, when Mario and Peach are alone, Bowser lifts her castle, overpowers Mario with his invincibility that he gained by stealing the Star Rod and imprisoning the seven Star Spirits; Mario is thrown away from the castle in the sky and lands in Goomba Village, where he is aided by Goombario and his family. --Wikipedia

I don't even know where to begin praising this game. 

In terms of ambition, it sets out to combine timeless RPG game elements such as leveling, questing, boss fights with a rich story, a cool inventory system, and even a party system.

But it also had this well-orchestrated aura of gentle puzzle-solving as Mario walks around in a world full of lively characters with lots of personality and a great sense of humor.

Oh and the *quick time events* that increased suspense during battles...simple but oh so effective!

All of this with the incredible aesthetic and amazing music threw me right off, I was hooked from day one.

The story and the conversations you have with people in this game are highly responsible for my increased development in English, no doubt.

![The Crystal King](https://www.mariowiki.com/images/d/d5/PMCrystalKing.png)

I have a strong memory of getting emotional the first time I ever beat the Crystal King, as I had spent weeks if not months not only gearing up but just trying to figure out how to get there in the first place.

Be kind, dear reader, I was only a boy.

And while I've been on the internet actively since the age of 8, there was no real walkthrough to speak of at that time...not that I could easily get my hands on it anyway... and the mood was somewhat different on the internet back then and so, it never really struck me as something I could do either.

But I didn't care all that much if I got stuck, *that's* how much I loved this game.

I even bought a green BradyGames strategy guide that came with a poster that I put up in my room.

If I attempted to pinpoint what makes Paper Mario work for me so well it's the fact that you explore a fully functioning and original RPG, with interesting character development, attacks, enemies, items, badges... and I could never quite swallow the Japanese pill... I understand that the objectively best RPG games that exist are the Japanese ones... but I could never get myself to invest time in worlds full of cocky boisterous boys with weird hair. For the very same reason, I grew to fall in love with the EarthBound series, as it too functions as an immersive and original introduction into RPG gameplay, but without the "overly Japanese" aesthetic.

#### Times change

I got older and eventually nagged my poor parents until I got myself a PlayStation 2. 

And even though I was an only child, you didn't get to own more than one console per generation unless you've won the lottery.

So my choice was made, and I don't regret it.

The GameCube seems to have some really solid titles on it, and it seems to have aged surprisingly well at that, but it doesn't hold *at all* against the crazy library that the PlayStation 2 came with.

Mario Sunshine wasn't convincing enough for me, I think I even played some in a game store and it didn't grab me the way Mario 64 had done years prior.

But then I saw that they were making Paper Mario 2, or rather, *Paper Mario: A Thousand Year Door*.

And that's where the itch started.

I tried talking my friends into getting it so that I could get my hands on it... time went by, I got older and interested in a lot of other things but every single time I heard the mention of the second title I always got a part of that itch back.

And so this year I scratched a good part of it.

I haven't gone to the moon yet, and I'm not the same person I was when I was a kid, but I enjoy *The Thousand Year Door* too.

The story is more immersive, more mature, with even greater humor than in the original.

The story arch isn't as great as the original, however, to me it feels like an ode or a remix of concepts that existed in the first installment.

Actually, it's funny how many structural similarities they still share even though the stories are completely separate and can be enjoyed separately... I guess that's what they went with.

To me the original wins in terms of world building and I completely agree with the fandom sentiment that follows:
"The perfect Paper Mario would have the mechanics and attention to detail of TTYD and the world building and story quality of the first installment."

I bet I'll find the time to complete *TTYD* soon.

I think I'll always favor the original installment, but I give *TTYD* a pass as a good second installment in the Paper Mario series.

#### Like I said, I've kept in touch with the Paper Mario games

And, unlike what I usually do, I've even kept up with *the fandom* to some extent.

And the overarching sentiment from the fandom seems to overlap with mine.

The main complaint is **"Where the hell did my RPG go?"**

After *TTYD* they made Super Paper Mario for the Nintendo Wii, which didn't exactly make sore... but it smelled a little bit like "Son, we've got Paper Mario at home"... *it's just not the same*.

Unlike previous installations in the series, it explores a dumbed-down but interactive style of gameplay, stripping away a lot of the classic RPG elements in favor of a platform-style experience.

In this manner it's almost kinder to honor the older installments by creating a lineage of Mario RPG games, favoring *Mario & Luigi*-installments and adding Mario RPG for the Super Nintendo Entertainment System, as the grandfather out of the titles, the one that started it all.

Hell, even Paper Mario sees a feature in one of the *Mario & Luigi* titles, Mario & Luigi: Paper Jam.

It seems clear that the only unifying feature as Paper Mario moved forward was to explore the aesthetic with one release per console.

I even read from some supposed developer that they had lost touch with trying to make a solid RPG experience for Paper Mario or something like that... and ever since I've just slowly given up hope with every subsequent release.

After Super Paper Mario came Sticker Saga, an installment for the Nintendo 3DS, in this title we see a slight return to a more classic Paper Mario, but only enough to infuriate any fan of the first two titles. Instead of items and predictable, tried and true RPG elements we see even attacks taking some sort of fleeting nature, almost like with card-based interactive RPGs.

I guess they're taking an experimental route... oh well.

And then Color Splash... I don't even know what to say about that.

**To collect myself a bit:**

Color Splash sees a revamp of the installment with a high focus on the puzzle elements of previous titles, turning Paper Mario into an idle cozy experience...that still falls short due to its lack of a solid battle system.

I guess they wanted to explore their medium further with the ~~failed proto-Switch~~ Wii U controller and so the battle mechanics resemble a card style even more than in Sticker Star. It's glib, it's dumbed down, it's an attempt to appease a younger audience, which *was* Miyamoto's original intention with the series, of course... but what a decline for any fan of the franchise that enjoyed the result that was the first and second installments.

And now...

Today I was really tired and so I decided to have a look at the new title for the Nintendo Switch by watching this [speedrun by Headstrong1290](https://invidious.snopyta.org/watch?v=nQGDzeuy5V4).

Yeah, what can I say? 

**Paper Mario: The Origami King**

It looks beautiful, very *aesthetically pleasing*.

I'm sure it's even somewhat of a fun game, as the titles before...

But it's a wonky cutesie platform puzzler, they have yanked every single aspect that made Paper Mario the game that it was *except* for the RPG aspects.

The battle system looks like an IQ-puzzle, very obnoxious for the sake of being novel.

A lot of focus on exploiting the graphical medium, the paper aesthetic...at the cost of placing the characters in natural or authentic habitats.

Jokes aren't as great as they used to... it feels dumbed down in vision and execution quite frankly.

I don't think that even 9 year old me would smile at the thought of the creative additions to the Mario universe getting replaced by origami elementals, tape, and a stapler...

I understand that they can't make the same game over and over again, but I can't help but feel that I'm looking at a game series that has lost its touch.

[Nintendo, look at this IGN.com poll](https://www.ign.com/articles/paper-mario-the-origami-king-review)! `<---`

**For the love of God just make a good Paper Mario game again.**

---

[I occasionally post my Paper Mario gameplay on my YouTube-channel, here's a link so that you can view my channel through Invidious.](https://invidious.snopyta.org/channel/UCa0xHQDkmyHUg5gRGLc8JTw)

---

# Fin.

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-11-14-MassacredMYPaperMario.png" gallery="gallery" caption="Art for this installment of LHTMM" >}}
