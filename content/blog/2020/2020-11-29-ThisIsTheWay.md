+++
author = "dotMavriQ"
title = "This Is The Way"
date = "2020-11-29"
description = "Rough times and Mandalorians"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2020-11-14-ThisIsTheWay"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-11-29-ThisIsTheWay.jpg"
alt = ""
stretch = ""
+++

The last couple of weeks have been rough. My maternal grandmother dying, my cousin ending up in the ER, nephew on the run, the insecurity surrounding travels when it comes to meeting Margarida...burying my grandmother...work... worrying about people in my surrounding in regards to Corona... It's been rough, to say the least!

But at least today I managed to be somewhat productive in the face of that.

I've been making significant improvements to the design of the blog. 

I have barely scraped the surface but some improvements include font switches (that still need to be properly set for web use), improvements to the site theme, and size adjustments (that now need to go through the media-query treatment in order to look good all across the board).

I'm glad I got at least a couple of hours of good work done on it and I can't wait to keep improving it.

Other than that I decided to listen to my good friends Erik and Phil as they've been telling me to check out [The Mandalorian](https://en.wikipedia.org/wiki/The_Mandalorian).

> The Mandalorian is an American space Western streaming television series created by Jon Favreau for Disney+. It premiered with the service's launch in November 2019 and is the first live-action series in the Star Wars franchise. It begins five years after the events of Return of the Jedi (1983) and stars Pedro Pascal as the title character, a lone bounty hunter who is hired to retrieve "The Child".

I've been a fan of the Star Wars universe for quite some time I think... to me, it really started with the Universe books, illustrated books describing all the characters in the universe... yes, even before the movies! 

I had all of them, as they were released alongside the release of the first prequel when I was a kid.

And I may not really have been won over by the new movies in the same way as I've felt for the franchise in the past but... it's OK.

What I really like about [The Mandalorian](https://en.wikipedia.org/wiki/The_Mandalorian) is how timely it is in its thematic choice. 
The hero is rather an anti-hero, and much like in classic Spaghetti Western there are bouts of choice-making and morality sprinkled all-throughout every episode.

I'm half-way through the first half of the first season of this series now and I'm really seeing some sort of `Erasploitation` or `Retrosploitation` going on that I've never seen before as explicitly as with this. What I mean is that they're deliberately going back to fetch the sounds that they conclusively *know* that the audience wants and go out of their way to bring back a *Golden Age* vibe that existed in the old movies by using old techniques on top of newer technologies. 

[The Mandalorian](https://en.wikipedia.org/wiki/The_Mandalorian) exists, cinematographically, as this fandom homage instead of going the more typical "Triple-A Movie"-route with all-new-everything and meticulous crowd-pleasing to the point where you can feel that the movie was directed more by a board juggling economic interests rather than being the creative vision of a single director. 

In this sense, it does what exploitation films did in that they knew what sold, and movies were shaped surrounding single points of real-life subject matter, mainly violence, as the American ESRB and the likes kept bigger studios from creating too much violent media. What I'm saying is that in the same way these movies exploited an audience's need for violence in order to make bank, *The Mandalorian* seemingly goes balls to the wall with nostalgia, old practical effects, tried and true soundbites... I know they've been doing it in the last couple of movies, like I said, but they're really going all-in with *The Mandalorian*.

We now have the latest and greatest CGI out there and yet people and film studios both seem to agree that there is something to the older practical effects, sound designs, and costumes that are worth keeping... It already existed in the last three movies but no new release since the first three movies have utilized as many old practical effects as The Mandalorian. It's arguably what outspoken fans have always wanted.

Phil told me in passing that they're using "Virtual sets" (A wall of LED screens) rather than a green screen and I agree that what they've managed is amazing.

Whenever you use a [green screen](https://en.wikipedia.org/wiki/Chroma_key) you get tons of green light emissions, colloquially called *spill*...and fixing all of it in post leaves this... lacking dynamic... or at least it seemingly makes it tougher to trick natural perception with it... whereas this technique seems to bring it home just fine! 

I, just like my friends, seem to enjoy The Mandalorian more than I've enjoyed the more recent movies... it was a smart strategy from Disney, for sure.

Well... Tomorrow's another week... I've been feeling some light flu-like symptoms throughout the day that I really don't hope will get any worse...

If it gets any worse I'll book time to get tested... I mean I've never tested for COVID so maybe it's time.

Hell, maybe I'm just decompressing from the stress I've been feeling, who knows?

Oh well, it'll get better

**This is the way**

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-11-29-ThisIsTheWay.jpg" gallery="gallery" caption="Some art I made for this post" >}}
