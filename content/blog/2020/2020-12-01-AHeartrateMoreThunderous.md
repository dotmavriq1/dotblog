+++
author = "dotMavriQ"
title = "A Heart rate More Thunderous"
date = "2020-12-01"
description = "Than Drums"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2020-12-01-AHeartrateMoreThunderous"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-12-01-AHeartrateMoreThunderous.jpg"
alt = ""
stretch = ""
+++

Today was a pretty cool day!... and I'm not sure where to start and in what order to talk about stuff so I think I'm just going to have this be a less structured post than usual... 

##### STAXX

I woke up, tended to work.

There was this.. possibility that the infamous Swedish gangster rapper turned [evangelical Christian](https://en.wikipedia.org/wiki/Livets_Ord) [Sebastian Stakset](https://en.wikipedia.org/wiki/Sebastian_Stakset) would show up as he apparently...maybe knows my boss? I'm not sure and quite frankly...I try to keep work and leisure separate so I won't be speculating or elaborating further on here...

...But what matters is that we were kinda stoked about the possibility of meeting this guy!

He's one of a kind, *for sure*. 

Grew up in relative hardship... got picked up by the criminal lifestyle, made some bad life choices, and only in recent years has he resurfaced as this purportedly warmhearted guy that has accepted Christ into his life... and that might not appeal to everyone, especially not his previous fanbase... but to me it sounds like a person I wouldn't mind having a Fika with... 

I had questions about if he knew people that I knew back in the days when songs were being spread on the internet et. c et. c.

And of course, I'd probably ask him about how it was to have a small part in the making of the Swedish comedy series [Percy Tårar](https://www.oppetarkiv.se/video/2818857/percy-tarar-avsnitt-4-av-6), as it is an absolute cult classic!

So yeah, I wanted to go, and so did my coworkers... so... we took the risk and joined forces at the office.

##### Ordered some Thai Food or whatever for lunch

We also tried this new [Foodora](https://sv.wikipedia.org/wiki/Foodora)-service restaurant called [Boba Tea](https://www.foodora.se/en/restaurant/f0ct/boba-tea?r=1) or whatever... and I ordered a *Pad Thai* because I'm boring.

We also ordered in some *Boba Tea*, me and Phil split one, which cost `60:- SEK` and eehr... yeah, tried the Passion Fruit variety... it wasn't worth it hahaha... to me it tasted like *Shisha Tobacco with slime and gristle*... *sorry Hipster girls*... I'm not on board.

The *Pad Thai* was sub-par to the point where the only thing that redeemed a price point of `95:- SEK` was the fact that I received well over 1kg of food... like after I finished it I had Niklas hold my *styrofoam clamshell* containing the food and we both agreed that it was probably roughly a kilo left.

I'd rather have half of that and have it taste better though, but that's just me.

##### Yeah but what about that rapper?

Well, the work hours kept coming and they d*o*n't stop *co*ming and t*h*ey d*o*n'*t* st*op* coming *a*nd the*y* don't stop coming...

And all of a sudden it was 5 o'clock and there was no Sebbe to be seen... maybe some other time, who knows! 

##### Philip Testar

I can't believe it... but my old *partner in crime* Phil was up to no good and so we recorded an episode of **Philip Testar** where he drinks a *really* old beer... like... I don't even wanna know... 8-9 years due? 

I'm surprised he didn't vomit, to be honest... I'll post it after I've done some editing.

*Oh Phil.*

Sat for another hour at work finishing up my day's work... it was a good day from a work perspective too, I got a lot of stuff done and I feel like I have a good grasp of what needs to be done, which always feels good.

##### Tiny adventure to the drugstore and beyond

I put on [The Symbolic World with Jonathan Pageau](https://thesymbolicworld.com/) and headed towards the big ICA in town, swung by the drugstore, went inside of ICA.

I did the not so Swedish thing of putting a mask on as I noticed a lot of people were finally wearing masks...in Sweden... and I could not help myself but not conform, which ironically enough is Swedish of me!

Anyway, I bought taco seasoning, a packet of cream and a Zeunerts Julmust.

Julmust is this traditional Christmas drink we Swedes drink in case you did not know already.

It's a lovely thing... it's everything the imperial swine that rule Coca-Cola Company wishes they could achieve...as a matter of fact, they market aggressively in Sweden during winter times specifically because they receive lower numbers of purchases or at least lost sales due to people buying Julmust instead...Take that!

I figured, with it being the first of December and all, that I could celebrate a good day with a classic Julmust.

I bought the stuff and went home.

##### Surprised by how tasty my tacos turned out

So I forgot to buy some sort of cold dairy product to make a cold sauce or whatever... like a sourcream (gräddfil or Creme Fraiche if you're Swedish), but hey... I was determined to make something tasty either way.

So I chopped some button mushrooms and an onion, let them soak in some lovely butter in the pan, and get some color before I added mince and seasoning to make it a little bit more exciting.

The tortilla bread was some cool product. 

They contained 45% vegetables or something?... they had nice texture at least!

And then I added nicely chopped bell pepper, [fre*sh* a*vo*cado](https://www.urbandictionary.com/define.php?term=fr-e-sh-a-voca-do), the habanero hot sauce I got and Castellos remarkably crumbly cheddar cheese which made for lovely, potent, and scrumptious little wraps that I feasted on. 

##### In the words of Porky Pig

That's all folks! ... you know... I digested the food, blinked, and all of a sudden it's `23:00` and time to talk to Margarida.

But that's always nice and it cheers me up. 

I love you, you little goof!

##### As melhoras Lita

Margarida told me that Lita, her maternal grandmother, had hurt herself and was in the hospital. 

I swear 2020 keeps finding devious ways of being even more horrible than it previously has been, and it keeps doing it exponentially, *apparently*.

I just hopes that she has a quick recovery. Amo-te muito Lita.


**Stay tuned!**
---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-12-01-Taco.jpg" gallery="gallery" caption="The delicious taco with the cool tortilla bread" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-12-01-AHeartrateMoreThunderous.jpg" gallery="gallery" caption="Some art I made for this post" >}}
