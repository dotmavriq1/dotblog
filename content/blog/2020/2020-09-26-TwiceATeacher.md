+++
author = "dotMavriQ"
title = "Twice a Teacher"
date = "2020-09-26"
description = "Today I tutored a young man in programming and aided my mother with her android phone"
tags = ["Life", "teaching"]
categories = ["Blog"]
aliases = ["2020-09-26-TwiceATeacher"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-09-26-twiceateacher.png"
alt = "Me being a teacher, wojak style"
stretch = ""
+++

I woke up on the couch but managed a good nights sleep either way.

Today I was going to tutor a young man who claims to be dead set on working in the field of programming and so I met up with him at the office, did *the corona-elbow-thing* and started lecturing. I think he might've been like 13 or 14 or something?

I wanted it to be more personal but also more job market oriented than what he can expect from future curricular endeavors and so I structured the day as following:

* Brief history of computing and networks
* What are the different job roles and what varies in terms of job assignments.
* Humble introductions to HTML, CSS and JavaScript.
* Tools of the trade with a focus on Git.
* Further covering the more technical sides of the field; SysAdmin, PHP, databases et.c.

By the end of the first hours of the day I felt like he was up to snuff and started grasping the most rudimentary aspects of what I am doing and what is to be expected from my field of work.

He was a respectful and eager young man and everything went smooth. 

By the end of the day I showed him what a Raspbeery Pi is, hopdfully opening his eyes to what a computer can be, what a server can be and how he can acquire cheap means of learning the trade at home without busting the bank.

I think it's important to disperse information to young people in a way that intuitively not only makes the information matter but to also bring up examples that involve engagement.

And I might be getting old but there really is something magical about teaching something you're passionate about to anyone eager to learn.

**Speaking of which**, by the end of the day I went from teaching coding to a young audience to teaching technology to my mom.

She invited me over for dinner and we had a look at how to use her smartphone more efficiently.

Meeting mom and dad is always nice and something that corona has made sure that I don't take for granted.

I got some steaks and roasted vegetables with a yoghurt sauce and yummy garden herbs.

My only nitpick with the day had nothing to do with people in particular but with the state of common technology.

My mom asked for a means of keeping notes on her *pad* that runs Android and being a lover of free and open source software I downloaded what seemed go be an android port of open office.

And my mom really liked the app and it made me really happy.

That is, until I realized that it was laden with advertisements. Screen-filling pop-ups when you close a document, banner on the bottom near the navigation menu...

I balled my fist in my hand...

Yes, of course I checked F-droid on my phone to see if there wasn't a better alternative... But there really isn't! I'm not gonna succeed in getting my mother to mess around with Markdown like... 

We have computers in our pockets with more capacity and hardware accessories than most first home PCs for anyone born in the 90s and yet it seems to be a feat of astronomic proportions to hand out a free office suite that neither tracks you nor landlocks you with logins to online accounts without also being filled with disgusting advertising. 

Ugh...

Well my mother drove me home and I fell asleep on the couch for a couple of hours, missing my shot at good conversation with wifey, but she seemed as drained as me.

Went for a shower and listened to podcast until it was time for bed.




##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-09-26-twiceateacher.png" gallery="gallery" caption="Art for this post" >}}
