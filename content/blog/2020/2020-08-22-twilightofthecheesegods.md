+++
author = "dotMavriQ"
title = "Twilight Of The Cheese Gods"
date = "2020-08-22"
description = "Toby, Margui and me go for a car trip"
tags = ["Life", "Cheese", "Toby"]
categories = ["Blog"]
aliases = ["2020-08-22-twilightofthecheesegods"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-08-22-TwilightOfTheCheeseGods-15.jpg"
alt = ""
stretch = ""
+++

( If you read yesterdays post, I ended up not becoming sick, and that's a blessing in and of itself.)

`10:00` sharp and Toby shows up outside our home. 

We get into the car and off we go. 

We take the first stop at Hornborgasjön, a famous place nearby that is famous for being populated by cranes during the spring, but the view is really beautiful and there was some interesting history to read up on. 

We also stopped by Gudhems Kyrka to look at the ruins and read some of the really interesting history of that place. 
You could pay to light a candle in commemoration of someone, so I lit a candle for my late grandfather.
There was also this bowl of water, where one could place stones to alleviate burdens, Margarida placed a stone in the water bowl. 





---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-01.jpg" gallery="gallery" caption="Inside Gudhems Kyrka" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-02.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-03.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-04.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-05.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-06.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-07.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-08.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-09.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-10.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-11.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-12.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-13.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-14.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-15.jpg" gallery="gallery" caption="dicpic" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-08-22-TwilightOfTheCheeseGods-16.jpg" gallery="gallery" caption="dicpic" >}}
