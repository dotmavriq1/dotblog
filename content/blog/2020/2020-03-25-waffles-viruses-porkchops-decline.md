+++
author = "dotMavriQ"
title = "Waffles, Viruses, Porkchops and Decline"
date = "2020-03-25"
description = "Why Swedes celebrate Waffle Day and where we might be heading"
tags = ["Life", "Corona", "Linux desktop"]
categories = ["Blog"]
aliases = ["2020-03-25-waffles-viruses-porkchops-decline"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-03-25-waffles-viruses-porkchops-decline.jpg"
alt = ""
stretch = ""
+++

##### I had a bit of a stressful morning in all honesty. 
Maybe I didn't catch enough sleep, not sure what it was, but I barely got to work on time.

Things did settle quickly enough though, and that's always good, there's a special kind of peace that comes with sitting down and focusing on work for a good couple of hours. I mean, not every day can be like that of course, but today was a day like that. 

Today is Waffle Day in Sweden, I've heard rumors that there would be waffles served around 2 pm at work. 
It made me think about why we even have a Waffle Day in the first place in Sweden.

##### So why do we celebrate Waffle Day in Sweden? 
The spring feast day(*vårfrudagen*), or the Virgin Mary Annunciation Day(*Jungru Marie Bebådelsedag*), is a church feast that has historically coincided with the beginning of spring farming and falls on March 25, which is nine months before Christmas and Jesus descent. According to the gospels, the Virgin Mary was told that day by Archangel Gabriel that she was pregnant.

It is believed that the name Waffle Day (or Våffeldagen, as we say in Swedish) comes from the pronunciation of Spring Day (*Vafferdag*) incorrectly. It turned into a Waffle Day, and from there it went on in some dialects to Waffle Day, and *vaffla* is an older variant of the word *våffla*. Thus, the **Waffle Day** was born. 

In addition, waffles were a good seasonal fit as they used to have more eggs and milk after the winter...

*Anyhow*, after some SEO and site-tech work it was time to clock for lunch. I put my headphones on and headed home. Sun is out this time of the year in Sweden. It's already looking really pretty and I like that. My inner trekking-wannabe has had an unscratched itch for the past couple of years and so I find myself drifting into thoughts of realizing a lot of what I've been setting out to do for this coming spring and summer. 

Had an uninspired and slightly burnt serving of Krögarpytt with some crème freiche... I've had worse, man vänjer sig.

Later the waffles that was promised was served. 
You gotta love the honesty in my workforce, everyone enjoying the company and vibe of the occasion but ultimately agreeing that the waffle mix was far too eggy and strange. Good vibes.

I did quickly head back to work though, I've been like that lately, not asocial per se, but rather enjoying getting things done, especially at work. 

##### LARBS is great

I am really enjoying my switch to use [LARBS](https://larbs.xyz/) on my work laptop. Nifty little bar for [i3](https://github.com/Airblader/i3) nicely configured everything... yeah, the only stuff I've modified is Swedish keyboard settings, xrandr settings and preferred applications. I'll be sure to post my configs on my gitea instance soon enough.

The work shift came to a close and I swung by the local Hemköp to buy some stuff.
I ended up buying avocado, porkchops and other goodies.

The button mushrooms and seasonal onions were on sale also so I bought those and went home.

I ended up making a version of the porkchop-recipe that Margarida makes for me. 

##### Get to the cough-stuff!
... so we're inevitably coming to a close with this blog post. And what better way to end it on than to bring up what has been on just about everybody's mind as of lately: **Corona**.

First I listened to [God Ton](https://soundcloud.com/godton/81-rektor-hamid) with Swedish politician Hanif Bali and his inner-city Stockholm sidekick. It was one of the better episodes they've posted, this time with Rektor Hamid, who was praised with the title Årets Svensk 2018 for his contributions to the Swedish educational crisis.

He talked about how he went about improving Swedish schools with rapidly declining grades and other types of problems, more or less exclusively related to bad integration measures on behalf of the Swedish government. Good episode if you understand Swedish!

Next episode was [Hur Kan Vi](https://poddtoppen.se/podcast/1387372894/hur-kan-vi), with digitalization expert Ashkan Fardost and the professor of complex systems. They discussed the future of society with threats like corona coming more and more into play. Even though the quality was potato for a podcast that I've usually applauded for quality I would still recommend the episode to anyone that understands Swedish.

...Now that we're going to spend more time than ever relying on internet sources while disconnecting from our regular scheduled social contacts like those from work, family, social circles and events I end up thinking a lot about Platos *The allegory of the Cave*.

The *'Allegory Of The Cave'* is a theory put forward by Plato, concerning human perception. Plato claimed that knowledge gained through the senses is no more than opinion and that, in order to have real knowledge, we must gain it through philosophical reasoning.

The easy thing to do would be to view the isolation as the cave, but I think about it like this instead: The "real cave" would manifest for those who try to use the internet to sense their surrounding in these trying times, rather than allowing reality to settle through philosophical reasoning. It is a time to make calculated decisions, that's for sure. Things need to be allowed to take time as we gather more information.

Will this make people reflect more about society? 

The future of said society? 

What system we are actually living in and is it ideal? *et.c.* 

The questions are endless. 
And as the Professor in complexity said in *Hur Kan Vi*:
" *It should be pointed out that most unusual circumstances have paradoxical effects, people finding themselves in situations they've never been in collectively will have benign effects as well, such as people rethinking what matters in a society.* " 

So we are currently confirming 18 deaths in Stockholm and counting.

A quick visit to Twitter showcased people who are infected yelling into cyberspace that it is by no means a regular flu, with some cases still being sick at day 26.

{{< twitter_simple 1243573577862901760 >}}

On Reddit there was this worried son in [/r/Portugal](https://old.reddit.com/r/Portugal/comments/fo6kds/question_my_parents_live_in_portugal_and_are/) asking if it was a good idea to keep making steps in moving his parents out of their home, allowing a minimum of 5 people go into their home to move property...Everyone was vehemently against it. 

It does make me happy to know that people are taking this more seriously.

[Europe can expect interesting times.](https://en.wikipedia.org/wiki/May_you_live_in_interesting_times)


Over and out.
