+++
author = "dotMavriQ"
categories = ["blog"]
comments = true
date = "2019-12-22T22:30:00+02:00"
description = "My second trip to Portugal"
location = "Cascais, Portugal"
slug = "portugal2pt1"
tags = ["life", "travel", "Portugal"]
title = "Portugal 2 - The Saudadening [Part 1]"
removeBlur = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Portugal2/Portugal2_01.jpg"
alt = ""
stretch = ""
+++

Woke up around 0800, brought my bag, stressed out the door to meet my dear mother who was waiting in the car.  
My nerves were surprisingly calm and collected, but I had taken the time to be as prepared as I could be, at least when it comes to packing and what to bring. ( Or.. at least I thought, turns out I forgot to bring socks...)

Road to the airport went quick, check-in went quick, customs... the lot.
I mean I did it with the company of my father and Toby only a few weeks prior and this time around I only had myself to worry about which was surprisingly calming.  

![Gothenburg airport](https://cloud.dotmavriq.life/core/preview?fileId=41312&x=1024&y=768&a=true)
  
Got a little bit nervous as time was closing in, didn't really have the time to pee as I got on the first plane but British Airlines from Gothenburg to Heathrow must be the easiest plane ride I've ever taken in my life.  

And when I was in **Heathrow** I was struck with the same excitement and wonder I used to have when traveling as kid. I even had Christmas tunes by Frank Sinatra as I went to the bathroom, it was great.  
  
Now, a more aware person would of course know that I was in Terminal 5 as I arrived and that I should be in Terminal 3... And I bet any British person that has been there is laughing to the skies as they read this but: All of the signs at Terminal 5 saying _Terminal 3_ ... Well, they're not helping.   
  
I went to the right from the exit, took the elevator to the bathroom, saw this lone woman surrounded by _cue-line cordon_ and thought I had gone the wrong way but ultimately realized that it was where I was supposed to be... And so I waited and waited while the cue that now existed where the previously lone woman was got smaller and smaller.  
  
I finally got a bus that took me to Terminal 3.  
  
Last time we flew from Sweden to Frankfurt and got struck by a bad extended lay-over and had to fly from Frankfurt to Munich only to get to Portugal far later than expected... _At least they compensated us in full._   
  
This time around I got lucky in that department but while in the UK I wasn't expecting a second customs when I've gone from Sweden to Heathrow, but there I was, in the cue again, doing what Swedes supposedly do best.  
  
One more time of ripping apart my bag, putting my stuff on the trolleys, taking off my belt.  
  
It all went fine, but I found that those 2 hours I had before the next plane went fast and by the time I got to my plane I only had roughly 30 minutes left to spare. By this time I was really hungry but the vending machine that had Kit-Kat wouldn't take my card so I had to settle for the Fanta that the first vending machine had to offer.   
  
By the time the second plane was air-bound it started to get a bit draining, not nerve-wrecking as I had awaited the travel to be... just... draining.   
  
As I finally arrived in Lisbon my spirits were back however as I was _finally going to see Margarida_.  
  
A little fidgeting and back-and-forth through some ID-checks et.c. and there she was.   
  
I held her like life counted on it and smelled her hair, _finally_.  
  
After that I met with Xana and Carlos and we got in the car and in a short window of time we were home.  
  
Shortly thereafter Nuno joined, nice as always to see him, we sat down and enjoyed some _coelho guisado_ and it was delicious!   
  
As the hours passed and good conversation was had it was time for me and Margarida to hit the bed.   
  
Boa noite  
  
_Vem bryr sig om hur nätterna far?_  
_- Jag bryr mig inte ett spår._

***
