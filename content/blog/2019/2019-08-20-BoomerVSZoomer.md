+++
author = "dotMavriQ"
categories = ["blog"]
comments = true
date = "2019-08-24T11:30:00"
description = "What contemporary memes tell us about ourselves"
location = "Västra Götaland, Sweden"
slug = "BoomerVSZoomer"
tags = ["rant", "Boomer", "Zoomer"]
title = "Boomer VS Zoomer"
removeBlur = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2019-08-24-ZoomerVSBoomers.jpg"
alt = ""
stretch = ""
+++

So I just got word that I am what can be considered a *" 30-year-old Boomer"*, something which is, *in these days*, colloquially shortened to Boomer.

Now we all know that the single most cringe thing one can do is to post "information" from [**KnowYourMeme**](https://knowyourmeme.com), **so here goes:**

> 30-Year-Old Boomer is a character spread on 4chan mocking older millennials who enjoy things that are considered to be out of touch with younger millennials, particularly in regards to video games. The phrase generally appears in a greentext story based on a snowclone reading "that 30-year-old boomer who X," accompanied by a specific Brainlet Wojak variation.

**So, in Boomerspeak:** 

Remember ["Rage comics"](https://knowyourmeme.com/memes/subcultures/rage-comics)? 

You know, that old meme with the angry face at the fourth frame? 

Well... [Wojaks](https://knowyourmeme.com/memes/wojak-feels-guy) are kind of the natural evolution of memes in that they are meant to be even more relatable. 

*Validity* and *Relevance* seems to be the two most prevalent factors at play in the internet age anyhow.

Wojak, originally known as "*The Feels Guy*" to most people on the internet, sort of developed into becoming an armchair study of class, background, and commentary on social demographics.

The Boomer, which would be the class of which I adhere to, at least culturally, is seen to be a person around the age of 30, that carries the exact same dreams, ambitions, and overarching analysis of the world as the generation of their parents. 

This means that a ** 30-Year-Old Boomer** has the mindset that life is about settling down, further securing their occupation, and buying toys that border on being *too* tacky means of entertainment, which is why he is seen playing Guitar Hero on his wall-mounted TV through incredibly expensive speakers.

He is very rarely married, but seemingly happy with his achievements either way, unlike other modern Wojak-renditions, such as the Doomer, a person that "has seen too much" and subsequently given up.

One of the most iconic displays of *The Boomer* is a visual of him mowing a lawn on a Lawnmower-truck.

![Guitar Hero Controllers - WikiPedia](https://upload.wikimedia.org/wikipedia/commons/c/cb/Guitar_hero_logo.png)

While I never caught on to Guitar Hero(, and ended up mostly annoyed that my friend spent so much time improving his GH-skills to the point of near-perfect results but wouldn't make the switch to actually learn a real instrument... or that his father bought every single edition of Guitar Hero, Rock Band, and all the corny quasi-instruments, straps and gear to play on his wall-mounted TV with God-Knows-How-Expensive Speakers causing something beyond the threshold of enjoyment and into tinnitus-inducing displays of speaker-prowess...), but I did spend a considerable amount of my savings on instruments and studio gear that I rarely ever get time to play with. 

And I do not own a lawnmower, nor a suburban house with grass to mow but I do have stable employment, it irks me that the politics are changing from what was meant to work for my generation, the one before me and all subsequent ones... and maybe I DO have a problem letting it go... and while I don't own obnoxious vehicles I guess one wouldn't hurt if I lived the way I wish I could at the moment, in a house on the countryside. 

You work to the bone so that you can enjoy the privilege of avoiding other people, freedom is the possibility of isolation and all that.

So I'm... by this analysis alone... some sort of liberalized social democrat, possibly browning somewhat at the edges but refusing to admit it, even to myself, wondering why the world is changing for the worse while enjoying some material excess, splurging in obnoxious and corny hobbies.

---

**Some more antics of the 30-Year-Old Boomer include:**
* [90s neologisms still sticking around](https://en.wikipedia.org/wiki/Category:1990_neologisms), still resorting to words like **Cool**, **dude** et.c.  
* Excess spending on Nostalgia (There's even a Nintendo-Boomer to further cement this cultural tendency!)
* Stable economy (At least lower middle class to middle-class lifestyle, no [McJob](https://en.wikipedia.org/wiki/McJob))
* A sense of music being better in the past ("What the hell is [**Trap**](https://en.wikipedia.org/wiki/Trap_music) and how do I end it?")
* A sense of movies being better in the past (Why are movies like triple-A games these days? What happened to the authority of the director?)
* A sense of games being better in the past (What the hell is a "Battle Royale", what is "bloom", why does everything have to be online? I'm not going to spend time "gaming" on my phone..are you kidding?)
* A sense of things being better in the past (Job market, politics, culture...you name it)

Lastly, *The Boomer* is seen drinking a Monster Zero, which I figure is meant to be a display of someone that naively thinks that replacing a bad habit with one removes *the arbitrary definition of what was bad with the habit in the first place* is still allowed to keep the habit.

"Zero sugar, dude!" `*sips*`

Understandably, however, there are several distinctions between the American *30YOB* and a Swedish one(even though the overarching themes of course share distinct similarities).

I think the best way to comprehend the realm that occupies The 30-Year-Old Boomer is to contrast it with **The Zoomer**. 

---

### The Zoomer

* A Zoomer is a colloquial definition of a person born in Generation Z.

* The Zoomer grows up in a constantly changing world, with comparably fewer perennial truths and heuristics that have pervaded with previous generations, even though the alienation was already happening with **Gen X** and/or with **The Millenials**.

* The internet is not a novel monolith with which one has seen it's humble beginnings, it is most likely the sole means of communication for a lot of people born in Gen Z. 

* Work security is increasingly scarce as we are experiencing an ever-growing [Precariat](https://en.wikipedia.org/wiki/Precariat) in the New Liberal West.

* They are not as pathologically ironic as the Millenials, but the thought of taking anything *too* seriously is still present, which may be a symptom pertaining more to the fact that they are comparably young compared to other generations still, at the time of writing.

* The moral panic brought on by a certain class among Millenials (**"[SJW's](https://en.wikipedia.org/wiki/Social_justice_warrior)"**) is also mostly seen as a joke akin to how the very same Millenials would view the more culturally fundamentalist Christians in years prior.

* Music (most likely "[Mumble Rap](https://en.wikipedia.org/wiki/Mumble_rap)" is made on a laptop and most likely uploaded to [Soundcloud](https://soundcloud.com/discover).

* Barely plays offline video games, Battle Royale-games are fun (Yay! [FortNite](https://en.wikipedia.org/wiki/Battle_royale_game)!)

#### Now, by contrast:

* A 30-Year-old Boomer longs for a good single-player experience when playing video games. 

* Having grown up in an age where video games were monolithic, we enjoyed non-changing directed experiences that rarely ever saw patches and games that did not require a "season pass".

A watershed moment was certainly when *World of Warcraft* entered the scene.

Some part of our generation even failed school as a direct result of the cultural impact brought forward by that game.

Other people, me included, *could never truly bother with it*.

Being "competitive in gaming" for a 30yob means beating your friend as he/she sits next to you on your couch with two controllers...at most you're *pwning* your friend over a LAN connection and with very very few exceptions you might even *pwn* them online. 

Bastard game genres didn't exist either, *Battle Royale* didn't exist, neither did *Hero Shooters*. 

If you compete in a game you compete in human skill, watering down that effort with means of awarding every player for engagement in the game (total playtime) is a cheap means for companies to reward whatever favors *them* instead of allowing the community to have an emergent scene with players that have a lead as a consequence of skill.

* Movies are pieces of art skillfully executed at the hands of a Director. It is the vision of the director, not a multi-billion dollar board. Movies weren't projects informed by data to attempt to appease every single living human being. Movies had integrity, even shitty ones.

(Of course, there is endless nuance being lost on the analysis, but that's kind of the point with all of these discussions and all this memery.
What I mean to say is that, while indie movies are now more popular than ever, we are seeing the death of giants like Scorsese in favor of things like...let's say anything Disney has touched on the big screen the past ten years.)

* The only rappers with the prefix "*Lil*" in their rap name which are accepted are *plausibly* Lil Jon and Lil Wayne, *if even*.

* We can still read books

#### In Sweden

* In Sweden there are no real 30YOB's that drink Monster Zero, and if they are they are most likely considered to be some variant of *White Trash*[.](https://sverigesradio.se/avsnitt/892927)

What we did have, and long for, is **Jolt Cola**, an integral part of the true nerd culture that followed with a profound interest in computers and all that came with it throughout the first decade of the 21st century.

* Zoomers drink **NOCCO** here

...in extreme quantities! Something about the aesthetic and gym culture being on the upswing for the youngins... but they're basically *"Red Bull with BCAA"* and no one could ever be taken seriously if they were to tell anyone that drinking that stuff has more benefits than avoiding them and replacing them with a better diet, the exact conundrum facing the previous generation that tricked themselves that Red Bull would keep them awake, alert and productive.

* Zoomer Cartoons are shit by comparison

Even before everything had to be 3D, a lot of Zoomers barely remember the **Hannah Barbera** stuff, the **Cartoon Network** stuff, **Looney Tunes**, **Turtles**... and worse yet, they probably picked up PokeMon somewhere around gen 4 or 5...*ugh*

Moral panic removed the more subtle adult themes sprinkled into the creative works of previous cartoons. 

No one is going to long for Shaun The Sheep the way that grown-ups of today nostalgically reminisce even the cheesiest, badly scripted cartoons like the **Teenage Mutant Ninja Turtles** of the late 1980s.

Granted they were even predominantly raised on cartoons at all, as childrens TV made a switch to "real" shows such as iCarly or worse.

* Zoomers don't drink, don't smoke and aren't getting laid.

The disparage in numbers of below-legal-age use of Alcohol, Tobacco and promiscous sex is vast compared to previous generations.

This is of course a good thing and I'd never contend anything but.

But. 

Flip it around for a second... What is so good, so prevalent and so strong within this culture that kids actively choose not to do any of these things? 

**The Computer**, **The Smartphone** et.c... 

And we've just started calculating the unseen consequences of this, keep a tab on the mental health numbers for years to come.

I am not at all advocating for the use of alcohol and tobacco when it comes to minors, neither am I a proponent of promiscuous sex for people that haven't even grown-up... 

But I am saying that it also means that there are several other things that young people are not doing, but arguably should.

Like hanging out, at all, in extreme cases.

* What is hidden in Snow... 

So they might not be out drinking and fighting... but an increasing number is medicated at a very early age.

We're all taking that route as a society... and it seems to be more of a consequence of sociocultural issues that need to be brought to life more than anything else.

"~~"But the priest says"~~ But the psychologist says he needs it" ...well... time may tell that it wasn't the correct trajectory to place society in is all I'm saying. 

Foucault and all that... [as well as that Simpssons episode](https://en.wikipedia.org/wiki/Brother%27s_Little_Helper).

#### Final words 

I'll finish with a shared sense I gathered with a good friend of mine. 

It seems that, where we're from, there is a change of heart among people born past 1995.

The ones born before rebelled against their parents and looked up to their brothers and sisters.

The ones born after the 1995 mark percieve their brothers and sisters to be morons and long for the lives of their parents, that are themselves notably younger than the previous groups parents.

How much truth there is to this neither of us know for sure...but it is an interesting observation nonetheless and some interesting food for thought...maybe I'll write more about it in the future.

I know this entire rant is laced with hot takes, struggling to be coherent... If you've made it this far, good on you!

I'm up for discussion with anyone willing to contend the truthisms and observations put forward by me and by the memes themselves. 

Take it all with a grain of salt, it's sort of meant to be consumed like that.

# OK Boomer