+++
author = "dotMavriQ"
title = "Duckstream"
date = "2021-02-04"
description = "A slow day, but watching Anders play was fun!"
tags = ["Life", "Corona", "JASP"]
categories = ["Blog"]
aliases = ["2021-02-04-Duckstream"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2020-02-04-DuckStream.jpg"
alt = ""
stretch = ""
+++
Today was just another slow Thursday.


I got some new job assignments that might fall through.


Mainly just doing maintenance work for a newly created site for a 
customer, making sure that old logs fit snugly into the new mold and all
 that.


We’ll see.


Hung out with Margarida and relaxed as much as possible in the times 
we’re in, I mean, Corona still ruins a lot of enjoyment, or worse yet, 
opportunities at all.


But let’s not focus on the bad parts of current living, shall we?


After work was done we prepared a nice meal and by happenstance, we saw that Anders was streaming on the [JASP channel](https://www.youtube.com/channel/UC2dW9DGQwwScVs-CUGBrhmw) he’s running with Simon and a bunch of his other friends.


That man was made to be placed in front of an audience through a screen.


He played [Quack Attack for the old GameBoy Color](https://en.wikipedia.org/wiki/Donald_Duck:_Goin%27_Quackers) and while I never played that one as a kid it was still really amusing to see not only another fine example of a platform game doing what it can be entertaining and challenging whilst still holding itself  within the technical limitations required for a game to run smoothly on a GBC cartridge.

It was a really fun and long stream that ended up with him getting the ideal ending and 100%-ing the game, props to you Anders!

Over and out.

---

##### Video for today:


{{< youtube m3UfosyB0aE >}}

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2020-02-04-DuckStream.jpg" gallery="gallery" caption="Art for this post" >}}
