+++
author = "dotMavriQ"
title = "Kafka Hell"
date = "2021-02-25"
description = "I can't deal with today"
tags = ["Life", "rant", "Corona"]
categories = ["Blog"]
aliases = ["2021-02-25-KafkaHell"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2021-02-25-KafkaHell.jpg"
alt = ""
stretch = ""
+++

I'm glad I've sat down to write this AFTER Margarida served me some beautiful quiche with a nice salad that slowly brings bliss to my worried mind.

If you're in the need of some brain-bleach after reading this post, I'll be sure to include pictures of the quiche at the end of the galler--ARRGH! I keep writing 2020 on the image files for this blog...STILL... like it's 2021 and has been for TWO MONTHS, GET IT TOGETHER.

**Ok... Deep breaths.**

Today was just one of those days, you know? 

I woke up feeling beat. Both me and Margarida have trouble sleeping, maybe we're getting a more collective case of *coronasomnia*... I mean it's not like we don't have worries swimming around in our heads quite often.

One last nice thing to report before I go on ranting though... Meditation, it does kick the llama's butt! I'm closing in on one full of month of nightly meditation with the Waking Up App and I can sense a difference after I've done it. I might even start doing it in the mornings and during my lunch breaks as it clears my head out in ways I can't explain. I can't recommend it enough.

#### Get to the rant! 

Work was alright, like I can't complain... but I've been in the mindset that sets in when you're low on sleep, feeling grumpy, and reflect on the world for too long.

Anyways, I needed to gather some small business emails or whatever.

Thing is, small businesses don't really...bother? If ever they've opted to just not use one these days.

For from all of them ignore it, it's not like it's the trend or whatever... but it's enough to point out that it's quite commonplace.

I notice that a lot of them have social media though, particularly **Facebook** and **Instagram**... that's where people are, undoubtedly.

So you have this federated technology that's arguably been around since even before the dawn of the internet, categorically speaking.

A technology that allows you to post a message to any place in the world, at any given moment, that can be read whenever the receiver so pleases, at the comfort of everyone involved.

Yet you choose to not engage in it... it might not even be *that* bad of a move, I mean... I kinda get it.

Social Media makes you feel like you're engaging more actively with your customers and several sources are indicating extreme increases in revenue just by creating chatbots that people interface with instead of attempting mass email campaigns and whatnot.

*I understand.*

It's just objectively the worse alternative.

Your email address can never be canceled, while social media can shut you down because ~3 people thought you were belittling their *Electromagnetic hypersensitivity*... I would know, it happened to my Facebook once... at least that's what I *think* happened as I never got an explanation that even told me what I had done within a frame of let's say 10-12 hours that could make three people file harassment against me, effectively forcing me to either quit Facebook and lose 2 years of established contacts, photos and timeline messaging, start a new account and risk having it shut down again, or open a case where I'd need to prove my innocence by accepting grotesque demands like photocopying my passport and connecting my phone number to the account for future reference, all of which could prove futile either way.

And I'm not painting myself fairly here if I state that I belittled them, I simply stated that Wi-Fi and Electricity aren't the same thing.
If you claim to have *Electromagnetic hypersensitivity* and you're reading this, I of course wish you the best of luck in life.

Beyond my ideological convictions regarding the horrors of contemporary social media that I have expressed in previous months, it should be abundantly obvious why email is superior.

_Just use a good provider, a good spam-filter, and a good client and nothing can beat it!_ 

* It is a virtue that email isn't direct, not all correspondence has to be.
* Federated social media is becoming a hip thing... Email was and will always be federated.
* It is a virtue to have software that you can control notifications on precisely as you see fit.
* Ever gotten spam on social media? Yeah... Can you filter it however you want? ... *No?* ... well...
* It is a virtue that you don't have a centralized platform with which to acquire contacts and that you need to either reply or know whom you're sending a message.

But... dear people reading this in 2021

Convenience, instant gratification, and aesthetics win out in the end... well, hopefully it doesn't but it puts a pit in my stomach just thinking about the trajectory.

#### Luddite addendum

And today I had to give up on my aspirations of dishing out as little information about me as possible while still tending to my work properly.

I've been using LibreWolf, a fork of Firefox with really good default privacy configurations as well as Brave, which might have some dubious practices, *arguably*, but still provide the best Chromium-engine browser out there, *arguably*.

Well... I couldn't do several things that I had scheduled today, like use nifty *browser plugins* or do certain things on customer websites because of limitations being put on browsers that block advertisement from a lot of websites.

I never thought the day would come where I'd **have** to use Google Chrome to properly view the internet, but it is most definitely here.

I just had to suck it up.

#### More Pagliacci, More!

I don't know where to start with this.

In July of last year, my beloved Margarida started to feel some pain in the right side of her lower back.

They made a formal diagnosis and she was put on antibiotics... fast forward to the *lovely* 2021 and she's now visited the ER on four different occasions. 

The first time the treatment *seemingly* worked, but came back with a vengeance towards the end of last year, and so towards the end of January, it was bad.

Here's the start of the carousel that is aiding someone who isn't a Swedish citizen in the Swedish public health care system.

She feels like she's been sitting on it to not worry me for a good couple of hours but she finally tells me that she's in pain and how convinced she is that it's not just a muscle pain.

She is of course scared which is completely understandable.

We call **1177** and they claim that with the symptoms she's showing she needs medical attention immediately.

We end up having to go to Skövde, I called my parents to arrange that, otherwise, we'd have to either deal with the pain until morning or wait until it's ambulance levels of bad which isn't exactly a fun situation to be in for Margarida.

Mom drives us in the middle of the night... We show up there... they take tests and hand out yet another medication plan.

A nurse and a doctor also tell her that **she did not need to go to Skövde if she was in Lidköping, that Lidköping would've taken her in either way**.

This has since been argued to not be true by my mother's coworker or whatever... what is objectively true is still unclear.

Actually many things have been argued from several sources or one source that have later turned out to not be true in this conundrum... goes to show what times we are living in really.

Her follow-up demands her leaving some tests in a Vårdcentral, or like a care unit that isn't an ER... 

In Sweden, we get one vårdcentral issued based on our place of residence.

If one does not have a permanent place of residence nor a Swedish social security number there is something called *Vårdavståndsprincipen* or something like that... Meaning that she is to apply herself in the one that is the closest to her.

This sounds quite easy, even though it's a nightmare to put in the lap of someone that isn't from Sweden... 

And you can't book Vårdcentral-time without a Swedish security number...lord knows I tried.

She faints one morning during her treatment and I have to wait 45 minutes in the **1177** cue line once more only to be told that fainting is serious enough to warrant yet another visit to the ER.

Ågården agrees with **1177** and we walk to the ER.

Third visit to the ER it turns out that they had access to tests taken from her second visit...however, none of that information was ever shared with her, me, or is accessible through any internet system that Margarida can access... 

So they change her treatment plan. 

Please note that even though this has been incredibly nerve-wracking and horrifying for us we've never felt like she's been in bad hands by any nurse or doctor.

**It's the system that is fermented bloody skunkpoop.**

All of these visits put us in a position where we find it to be the best fitting solution to have her stay in Sweden, as Corona threat levels have increased and demands on PCR-testing become mandatory for her to enter Portugal without a fine.

A PCR test needs to be taken within 48 hours (later 72, at the time of your reading it's most likely revoked). So you pay `2000 SEK`, it might be a futile attempt and a waste of money if you're Covid-19 positive if you're not you're able to travel within that frame of 48 hours. Should you fail to enter Portugal within this time there's a fine to be paid... *Wow...* 

And getting Margarida even sicker is of course nothing we want, so we took the sane decision.

And it's been a couple of weeks now, her symptoms magically disappeared on the second or third day of the new treatment and we've been calm and happy about it ever since.

##### and they don't stop coming and they don't stop coming and they don't stop coming 

But she's a modern woman, and like a lot of modern women, she uses contraceptives. 

You already know where this is going.

I will keep it short.

So I call **1177** for the fourth or fifth time since her first hospitalization in 2020. 

Patiently wait for 45+ minutes before I get a response, inform about the situation.

The ambiguity and lack of a clear answer to how we are to tackle this is quite staggering... and I *do* think that the nurse we talked to attempted to help us to the very best of her abilities.

We ended up on a miracle slide where she finally re-reads Margaridas age and realizes that Margarida is at least young enough to be tended to by Ungdomsmottagningen I Lidköping. She gets a time, goes there, and gets a prescription.

So many things factored into that not going well... the fact that Portugal and Sweden had the same medication... the fact that Margarida isn't my age as she wouldn't be able to receive care from Ungdomsmottagningen then... it blows my mind... 

I know I'm missing so much, and that I've also posted about a lot of this before it's just ... 

The final straw was today.

We still haven't figured out where she is supposed to leave her last test.

I call the only *Vårdcentral* that I haven't called, Guldvingen.

Really lovely nurse answers... and we have a talk that's so funny that we just laugh at me and Margaridas's situation because there's not much else to do.

She states that NÄRAKUTEN, which is a place that is a DE FACTO VÅRDCENTRAL, running under another name as it works as "an extended arm", renamed and switched building, which might be why we haven't been tipped by any nurse or anyone at all to go there, but it should be the place where we go to leave the test as they take in any admittance, even people that don't have a Swedish security number.

Not during any of the ER visits, nor under any of the **1177** phone calls has anyone ever mentioned this...not after the ER visits...nothing.

**BAD PRACTISE**

Parts of what she said sound so twisted to me, like if Margarida needed to leave a urine test she could do so by just doing it at home in any sterile plastic container... Do people casually have those? What jar would be clean enough? How do you gauge that?

It felt like I was talking to someone as pragmatic as a car mechanic...but it's my partner.

It's been a wild ride.

I'm just worried that we will miss out on our opportunity to meet some friends for the second time since Margarida got here tomorrow because we don't know how horrible the cue line will be at Närakuten.

#### Final blows

As Margarida started to make dinner today, and I have to work from home, it was hard to figure out how she'd go get test materials from a drug store that has the one she needs. Turns out only Apotek Näckrosen has it... where my parents live. So I called my mom and pleaded and she was kind enough to help us out.

Otherwise, Margarida would've had to halt making dinner and in the frame of that, we'd have to get the test, get back, cook, eat, do the test, go to Närakuten and drop off the test.

My mom just casually tells me that she has texted me several times throughout the week so apparently my mobile phone operator has faulty instructions for my cellphones APN-configurations most likely.

And Vimla, **the mobile phone operator does not have a phone number**. Just... reflect on this... I understand that the low price point has to do with cutbacks in these regions but... Just reflect on it for a brief moment.

Googling it actually gave me a hilarious response. Apparently I should call `070-0000000`...

Oh, and while we desperately looked for means to get her to buy medication or apply for more via **1177**, she looked into European Union solutions... She found herself that it seemingly supports eID, and that Portugal is one of the few countries that support it, so she patiently waited a week for the PIN to arrive, she types it into **1177**... ... ...  only to get a personalized message saying her name as they tell her that the service isn't supported on the platform yet. *WOW!*

And there's been heavier matters underlining all of these events, such as sickness among friends and family, be they related to Corona or not. And it's really heavy to deal with from moment to moment.

---

> The proportion of things stacking against us have me thinking about Falling Down, Kafka Novels and the movie *Brazil*.

Eh... I'll read some more of the Factfulness book I'm reading, do my daily meditation and after the test has been dropped off there might even be some time over to play some Witcher 3 or something... just turn off my head properly for a little moment, I'm pretty sure I need that today.

It's just one of those days where you want to plead to nature and humanity: 

## Mecum Omnes Plangite

---

**HAHAHAHA**... Wait people, listen to this... so Margarida just finished her class and we started to prepare to leave, I figured it would be good to call Närakuten or Närhälsan whatever it is to make sure that they're open before we leave, possibly to even tell them that we simply want to just leave a test and that arrangements might be put in place so that we don't have to wait in line and possibly miss the chance to leave it today if we have to stand in line... well... the local telephone numbers went to a cellphone number...without a message, yet prompting one to leave a voicemail... No way to confirm if it's actually the phone line to Närakuten. *Lovely*.

We still have no idea how tomorrow will turn out, knock on wood it'll all work out in the end.

---

**UPDATE:** We went there!
We thought for a brief moment that it was the buildings next to the old watertower, but that was *Närhälsan for midwives*... 
We walked back to the main hospital building half a block away and rang the doorbell/phone there, they stated that we were squeezing it a bit in terms of time but decided to let us in. Thanks!

It took a while to navigate there... Me and Margarida spoke about the possibility of the Skövde doctors speaking of *this* place having been available to us and not the actual ER...but the closing hours differ with about an hour... so I really can't tell.

They allowed us in, nurse was lovely as they tend to be.

We briefed her, provided a test sample.

After a while she came back and said that she struggled to understand what she was to file Margarida under, to no fault of the nurse.
She said that a doctors appointment is quite costly, even for EU-citizens today and gave us some test results back, everything points to the fact that Margarida is cured. Tack!

You know what?

maybe today wasn't all that bad after all.


##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2021-02-25-KafkaHell.jpg" gallery="gallery" caption="The proportion of things stacking against us have me thinking about Falling Down, Kafka Novels and the movie Brazil." >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2021-02-25-KafkaHell_2.jpg" gallery="gallery" caption="Queridas lovely spinace and cheese quiche!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2021-02-25-KafkaHell_3.jpg" gallery="gallery" caption="Queridas lovely spinace and cheese quiche!" >}}