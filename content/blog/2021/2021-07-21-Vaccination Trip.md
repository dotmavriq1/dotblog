+++
author = "dotMavriQ"
title = "Vaccination Trip"
date = "2021-07-21"
description = "Time to go to Trollhättan and get vaccinated"
tags = ["Life", "Corona"]
categories = ["Blog"]
aliases = ["2021-07-21-VaccinationTrip"]
removeBlur = false
comments = true

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2021-02-25-KafkaHell.jpg"
alt = ""
stretch = ""
+++

Even though today might seem like just any other work day, it isn't! 

Because today I started working early as well as skipping lunch so that I could take a 14:20 bus to Trollhättan in order to get vaccinated.

All of the preparation that is needed in order to reunite with my dear Margarida is tedious but always worth it.

And I got to rock my new Thin Lizzy shirt, that's always something!

Bus was long and winding... usually I'm not that bothered but the heat was coming on really strong today.

#### Arriving 
After arriving in Trollhättan I knew that I had to be swift in order to get to the appointment on time.
I had to leave the bus station and then try to get to Storgatan 47 as soon as possible. 

I walked down Drottninggatan, *Google Maps in hand*, and eventually I ended up where I should be..with like 10 minutes or so left to spare.

I flashed my app at the door, said "No, I do not have any cold symptoms" through my incredibly moist facemask, waited for what must've been less than a minute, talked to the second nurse, moved to booth no.2 and after I filled in a paper it must've been less than 8 seconds between the second she lifted the syringe and the second it was all done... well... now you have to sit and wait for 15 minutes so that you don't get any extreme adverse reaction to the vaccine. I waited and waited and as it clocked on 15 stat I immediately got out of there because the AC was crap during a really hot day and so... walking out of the Hotel building and breathing some fresh air near the water was reinvigorating. 

I had a strange walk around the city... something was clearly affecting me, the lot of it could of course be placebo... but I walked around inner city Trollhättan with this strange feeling. I kept thinking about how I couldn't even have dreamt this scenario up in my head less than three years ago. 






##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2021-02-25-KafkaHell.jpg" gallery="gallery" caption="The proportion of things stacking against us have me thinking about Falling Down, Kafka Novels and the movie Brazil." >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2021-02-25-KafkaHell_2.jpg" gallery="gallery" caption="Queridas lovely spinace and cheese quiche!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog" file="2021-02-25-KafkaHell_3.jpg" gallery="gallery" caption="Queridas lovely spinace and cheese quiche!" >}}