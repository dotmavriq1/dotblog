
+++
author = "dotMavriQ"
title = "Redemption"
date = "2021-01-09"
description = "The broken dishwasher saga continues"
tags = ["Life", "Corona", "housekeeping"]
categories = ["Blog"]
aliases = ["2021-01-09-redemption"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/2021-01-09-redemption.jpg"
alt = ""
stretch = ""
+++

So, in case you haven't read yesterday's entry, last night was a complete dud as far as ambitions are concerned.

I managed to organize a meetup with a friend of mine to dispose of my old dishwasher and travel to Skövde to fetch a new one. We said that we'd have our revenge and I booked a dishwasher online, as I expect someone did to ruin yesterday's purchase for me. 

#### Survival of the updated.

No need to lie, I've been sleeping shoddily ever since I left Portugal. Something about leaving the comforts of your loved one to tend to the unwanted normalcy that is being a grass widow in Swedish solitude just sucks the air out of me. 

I'm glad that won't have to last forever.  

So I woke up pretty beat around 08:50.  

Wrote to Phil, suited up, and head out of my flat to meet with the lovely Cherokee Jeep that was now parked outside my home.

We start by going to the garbage lot, it all went by quickly. 

Dropped off the old one and headed towards Trollhättan. The road was beautiful. Swedish countryside, morning mist, snow-covered forests. We turn up at the store, I ask a girl that works there how I should approach the purchase, she directs me to the cashier. 

I show them my order number from the email, wait a little. And there it is! What a journey for a little kitchen top dishwasher. We visit Dollarstore too, as Phil was looking for a frying pan. We find cheap ceramic ones and I could help but snagging one too, as well as some decent cutlery, a cheese slicer, and a pizza slicer.

And for the second day in a row, I harm my New Years Resolution somewhat by opting for yet another visit to MAX, as the day before. 

I was actually blown away just by how much worse this burger was to the exact same burger just the day before in Skövde.

There was really a significant difference. 

One might quality control all one wants when running franchises, but some things still slip through standardization, it was objectively less tasty, shoddily prepared and shoddily put together.

Whenever I need to scratch my urges for a fastfood burger, I certainly won't be doing it in Trollhättan from here on out. 

