+++
author = "dotMavriQ"
title = "Quesadillas"
date = "2012-11-10"
description = "And how to make them"
tags = ["Life", "Food"]
categories = ["Blog"]
aliases = ["2012-11-10-Quesadillas"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(26).jpg"
alt = ""
stretch = ""
+++

They've got fried bread, cheese and tasty things, what's not to love? 

I make these for myself more than I'd like to admit... and now even my sisters kids want me to make them every time I visit, hahah!

> A quesadilla is a Mexican dish and type of sandwich, consisting of a tortilla that is filled primarily with cheese, and sometimes meats and spices, and then cooked on a griddle. Traditionally, a corn tortilla is used, but it can also be made with a flour tortilla, particularly in northern Mexico and the United States.
> A full quesadilla is made with two tortillas that hold a layer of cheese between them. A half is a single tortilla that has been filled with cheese and folded into a half-moon shape.

So how do I make mine?

**Well... I mix the following into a bowl:**
 
* A squeeze of tomato puree
* Oregano
* Minced garlic
* Olive oil
* Salt
* Pepper
* A splash of really cold water
* Splash of hot sauce if you've got it

Whisk with a fork or whatever until completely mixed! 

I think I've stolen this style of impromptu tomato sauce from my dear father, credit where credit is due.

Now, take a tortilla bread, spread an even coating of the sauce... and then you add whatever you want inside of it... if you're making a "full quesedilla" using two tortilla breads I highly suggest not going overboard with the filling as flipping could turn out to be a shore with higher risk than reward... but you know... anarchy bro, go for it.

Yeah, just put some cheese in there.. if you got fresh herbs, pop it in! 

I typically add **bacon**, thin slices of **paprica** if I got it ... whatever is in the home that I think would make for a delicious quesadilla.

If you're a Mexican reading this, feeling greatly offended...well... I'm sort of sorry...but not really... because it tastes SO good!

I can buy you a shot of tequilla while you show me how the OG's do it, no problem.

... I'm rambling... **Let's eat!**

Oh and don't miss the cute picture of Lindas cat, gave them a visit today and whatnot.

---

##### Images for todays post:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(25).jpg" gallery="gallery" caption="Should look a bit like this!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(26).jpg" gallery="gallery" caption="Fried and ready for consumption!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(27).jpg" gallery="gallery" caption="Met this dude today also!" >}}
