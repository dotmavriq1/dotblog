+++
author = "dotMavriQ"
title = "Colder times two"
date = "2012-10-08"
description = "But today we got some sun at least!"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2012-10-8"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(9).jpg"
alt = ""
stretch = ""
+++

Here's the meal of the day. Roasted potatoes with eggs, bacon, cottage cheese, some of those greens too of course!

Some days you just waste away behind the computer, you know? 

Today was for sure one of those days.

Can't really tell you if anything was interesting enough to report on either...

Well, I'll try to report on interesting things in the days to come, stay tuned!

---

##### Images taken throughout the day:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(9).jpg" gallery="gallery" caption="omnomnomnomnomnomnom" >}}
