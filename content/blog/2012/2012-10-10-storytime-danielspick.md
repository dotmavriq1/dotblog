+++
author = "dotMavriQ"
title = "Daniels Pick"
date = "2012-10-10"
description = "How I accidentally caught the pick at the Vildhjarta show"
tags = ["Life", "thall", "Music"]
categories = ["Blog"]
aliases = ["2012-10-10"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(10).jpg"
alt = ""
stretch = ""
+++

I haven't really talked about the weirdest coincidence ever when it comes to seeing one of my favorite bands.

The band is [**Vildhjarta**](https://en.wikipedia.org/wiki/Vildhjarta), they play this style of [djent](https://en.wikipedia.org/wiki/Djent), I guess(?) but it's more like a band that has picked up on what Meshuggah did around Catch 33, fused it with odd-ball tunings, strange melody structures, beautiful layering and a juxtaposition between downtuned heft and bonechilling dissonance.

I love this band so much, and having the priviledge to have talked to some of the guys online is beyond me.

They're really cool dudes.

Anyhow... Metaltown, on the 12th of June this year.

They're playing the end of Deceit if I recall correctly... and as the silence comes on before the last burst of heavy *riffethy*, I feel a sharp pain on my neck. 

Daniel, the little bugger, flicked his pick really hard out into the audience, and it caught my neck!

It actually took me until after the show when I checked it a mosquito had bitten me or something to realize that on my right collarbone, underneath my shirt, was a brown Martin co pick...recently played and all.

I wrote to Daniel and he confirmed that it was his...

Crazy stuff... just absolutely crazy stuff... one of the best shows I've ever seen!

[**Here's the SetList for that night:**](https://www.setlist.fm/setlist/vildhjarta/2012/goteborg-galopp-gothenburg-sweden-33df284d.html)

```
1.Benblåst
2.Shadow
3.Eternal Golden Monk
4.Dagger
5.Traces
6.Pieces
7.Deceit
9.All These Feelings 
```

---

##### Images for todays post:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="Vildhjarta.png" gallery="gallery" caption="Daniels pick" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(10).jpg" gallery="gallery" caption="Daniel playing at the show" >}}
