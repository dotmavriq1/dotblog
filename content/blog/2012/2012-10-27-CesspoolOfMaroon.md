+++
author = "dotMavriQ"
title = "Cesspool of Maroon"
date = "2012-10-27"
description = "There.Is.No.Way.That.You.Can.Love.Her.Like.I.Do"
tags = ["Life", "Z"]
categories = ["Blog"]
aliases = ["2012-10-27-CesspoolOfMaroon"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(21).jpg"
alt = ""
stretch = ""
+++

I went to Lindas place to get drunk and hang out with Zira.

There's some people here and stuff... but I'm mostly interested in playing with the cat and blasting some vinyls.

Z finally got his hands on this beautiful **Pain of Salvation** release.

I read through the liner notes that had some really nice intel into the recording process of it.

**Road Salt I & II** will forever be some of my absolute favorite records of all time.

They depict life, struggle and pain in a way that few other albums nail quite as elloquently in my opinion.

And they're also fun to listen to, not a single track is shit, which is rare for bands that have diverse songwriting!

I find it extra funny that a more explicit extended version of the first track exists, but it's not on this vinyl.

**The extended part goes:**

> There is no way that you could touch her like I do.
Oh no.
She thinks of me when she's with you.
She wants it gentle like a child,
but play her right and she goes wild.
But one step wrong and she will hide.
And you will never touch her like I do.
**There is no way that you can fuck her like I can.
Oh no.
You're simply not that kind of man.
Cause sometimes when she's screaming no,
she really wants for you to go go go.
But you can never ask her why,
cause she will close up and deny.**

Ok *Mr.Gildenlöw*... geez... 

---

##### Images for todays post:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(21).jpg" gallery="gallery" caption="You'd have something that I don't? ...Would that be possible? " >}}
