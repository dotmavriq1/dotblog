+++
author = "dotMavriQ"
title = "Dinner with the vegan"
date = "2012-09-27"
description = "Yet another slice of life"
tags = ["Life", "vegan", "Z"]
categories = ["Blog"]
aliases = ["2012-09-27"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(2).jpg"
alt = ""
stretch = ""
+++

Spent the day hanging out with my good friend Z. 

We've been making some plans to get some sort of musical endeavor together.

Meanwhile, as he is vegan, we enjoyed some really good food together.

It was falafel and homemade oven fries and some neat vegetables we could scramble together from my fridge.

All in all it was a good day.

And my dude just colored his hair too! 

It's looking really neat.

**Anyways... I'll keep you guys updated!**

---

##### Images taken throughout the day:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(2).jpg" gallery="gallery" caption="Dinneeer" >}}
