+++
author = "dotMavriQ"
title = "Staying at my sisters"
date = "2012-09-25"
description = "just a slice of life"
tags = ["Life", "Onkel"]
categories = ["Blog"]
aliases = ["2012-09-25"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(1).jpg"
alt = ""
stretch = ""
+++

Just a quick update! 

I'm spending the night over at my sisters place as I am "babysitting" her kids or whatever.

Which we all know is just an excuse for me to act as a kid myself.

There's been videogames and movies and spaghetti carbonara...all of the usual suspects!

Aah... the bed down her is great though! 

I need my bedroom to be chill in order to catch some good sleep and it's really nice downstairs at my sisters place at the moment.

Feel like I'm going to sleep like a kid. 

**Goodnight people!**

---

##### Images taken throughout the day:

{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(1).jpg" gallery="gallery" caption="#Selfie from the bed" >}}
