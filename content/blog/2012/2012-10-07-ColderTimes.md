+++
author = "dotMavriQ"
title = "Colder times"
date = "2012-10-07"
description = "It's starting to get colder outside"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2012-10-7"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(8).jpg"
alt = ""
stretch = ""
+++

It's starting to get colder outside.

I never minded the cold as much as... I don't know? Maybe I get more gloomy and introverted whenever the sun isn't there to breathe life into my being.

I've been keeping at it with eating well at least.

Today I had these sandwiches with cottagee cheese, fried eggs and some beautifully crisp bacon.

Adding a little garlic to cottage cheese is for sure the trick to make it sing.

You know... it might not aid with the ladies unless you brush your teeth and all that... but hahah... it's so delicious!

Well.. I'm pretty sure that was it for today.

---

##### Images taken throughout the day:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(8).jpg" gallery="gallery" caption="omnomnomnomnomnomnom" >}}
