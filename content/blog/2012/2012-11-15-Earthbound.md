+++
author = "dotMavriQ"
title = "Earthbound"
date = "2012-11-15"
description = "with some food sprinkled in, of course"
tags = ["Life", "RetroGaming"]
categories = ["Blog"]
aliases = ["2012-11-15-Earthbound"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(30).jpg"
alt = ""
stretch = "horizontal"
+++

I've been informed on the fact that it is one of the most unique and well-written RPG's of its era for quite some time, and it feels so good to finally be able to sit down and play through it properly.

If you've been living under a rock for wha...A good couple of years, Earthbound entered the European and American market for the 16bit SNES console, or the *Super Nintendo*, as we also call it, in the middle of the of the 1990s. What people in the west didn't know at the time was that Earthbound was a sequel in a series of games, namely the **Mother**-series. 

> Themed around an idiosyncratic portrayal of Americana and Western culture, EarthBound subverted popular role-playing game traditions by featuring a real world setting while parodying numerous staples of the genre. Itoi wanted the game to reach non-gamers with its intentionally goofy tone; for example, the player uses items such as the Pencil Eraser to remove pencil statues, experiences in-game hallucinations, and battles piles of vomit, taxi cabs, and walking nooses. For its American release, the game was marketed with a $2 million promotional campaign that sardonically proclaimed "this game stinks". Additionally, the game's puns and humor were reworked by localizer Marcus Lindblom, and the title was changed from Mother 2 to avoid confusion about what it was a sequel to. 

What brings it home for me is the fact it doesn't look like most Japan-made role playing games... there's no dude in tights with spiky hair yelling from the top of his lungs, no gun-weilding princesses that can fly... you're just this boy in this town with his friends.

It had everything going for it but still didn't see as big of a spread initially as people didn't understand the humor that was used to sell the game.

Apparently a magazine even featured scratchable parts of the magazine that would smell really bad?

Anyway... the battle system is awesome, the music is even more awesome and the story is amazing!

I highly recommend any fan of old school videogames to check this one out!

I'm also including some stuff I've been eating lately because you guys seem to like pictures of food judging by the reactions I'm getting on Instagram or whatever... 

**Toodeeloo!**


---

##### Images for todays post:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(30).jpg" gallery="gallery" caption="Best RPG ever...after Paper Mario for N64...I think" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(31).jpg" gallery="gallery" caption="This was bueno" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(32).jpg" gallery="gallery" caption="Been listening a lot to Mutemath lately, this is my favorite record... oh and that food was amazing!" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(53).jpg" gallery="gallery" caption="Earthbound" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(56).jpg" gallery="gallery" caption="Earthbound" >}}
{{< fancybox hidden="true" path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(48).jpg" gallery="gallery" caption="Earthbound" >}}
