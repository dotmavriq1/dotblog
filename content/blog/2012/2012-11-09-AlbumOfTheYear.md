+++
author = "dotMavriQ"
title = "Album of The Year"
date = "2012-11-09"
description = "Yellow & Green Album by Baroness"
tags = ["Life", "Music"]
categories = ["Blog"]
aliases = ["2012-11-09-AlbumOfTheYear"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(24).jpg"
alt = ""
stretch = ""
+++

I don't even know where to start with this album... it must be some of the best music to have ever graced this planet since ever.

Baroness is a group that I've grown really fond of lately.
 
Stuff from their earlier releases reminisce **Mastodon** for sure, which can never ever be a bad thing... but I remember hearing *Jake Leg* from the **Blue Record** and I've been hooked on this band ever since.

They mix an eclectic style of rock with the more heavy and beat up sounds of sludge metal... there's some **Thin Lizzy** in there for sure... vintage speakers... beautiful roaring drums... emotional vocals and timeless lyrics... I... It's impossible for music to get as good as this... I still haven't properly digested it and I probably never will.

#### Yellow Album

From the intro to the first record we go into a straight banger, **Take My Bones Away**, the bassline is epic in this! 
Around 2:20 it just goes haywire with a beautiful display of musicianship and emotional intensity that just has to be heard to be understood!

This record keeps the emotional intensity up on tracks like **March to the Sea**, which is a personal favorite, **Little Things** that evoke emotions I can't share on here...**Twinkler** grants us a more acoustically oriented break as we segway into a beautifully retro **Cocainum** until we finally reach **Back Where I Belong** which is another one of those songs on the record that evokes astronomical levels of *Feels* inside of me.

**Sea Lungs** mixes the intensity of tracks like *Take My Bones Away* but adds a thumping drumbeat and resolving chorus... awesome stuff! 

And then we've finally reached the beat up glockenspiel feels-fest that is **Eula** ... just listen to it, there's no point in me attempting to describe it... fuck it.

---

#### Green Album

This intro right here... is one of the... *ugh* ... *I'm chopping onions as I start to write this*... it's the soundtrack to the joys of life itself and being with the one you want to be with, botherless... in a field of green, surrounded by nothing but the serenity of nature and love..  As **01:31** hits you, you down the first couple of swigs of your favorite beer, served ice cold as the sun smiles at you.

Oh you think that's cringe? Well give it a listen before you judge me will ya!

**Board up the house** has a really cool build-up as the latter record sort of flirts more with pop rock writing than the previous record... in ways.

**Mtns.(The Crown and The Anchor)** is a whiskey lullabye if there ever was one and the drunken sound only continues on into **Foolsong** before **Collapse** lulls you along in stoned serenity before **Psalms Alive** brings a trippy drum-centric tune that is really amusing to listen to.

**Stretchmarker** has these beautiful passages that bring back...possibly a nostalgia for whatever you felt or reminisce as you hear the intro...magical!

**The Line Between** picks up the pace with some really interested distorted and modulated leads caressing all around before the album finishes, without overstaying in the least, with **If I Ever Forget Thee, Lowcountry**, a serene outro that makes me feel withered and empty... maybe I won't be able to relive what I felt when I heard the intro...like ever... 

Deep dude... I know... almost too deep... 

Yet here I am...listening to this album for what must be the 50th or 60th time already... What a release... What a band... What a journey.




---

##### Images for todays post:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(24).jpg" gallery="gallery" caption="Let me know, when you will let me go" >}}
