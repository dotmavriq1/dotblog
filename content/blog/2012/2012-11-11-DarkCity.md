+++
author = "dotMavriQ"
title = "Dark City"
date = "2012-11-11"
description = "Dead Man"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2012-11-11-DarkCity"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(28).jpg"
alt = ""
stretch = ""
+++

I've been sleeping like complete shit lately. Something I've arguably always done... but it keeps getting worse.

And what usually happens is I try to sleep, can't... and walk around like a dull zombie for hours until I either give up and sleep some in the middle of the day or I just power through the entire day with very limited cognition...something I've learned to almost enjoy, no matter how weird that sounds... 

Well there's been some new developments... I've been starting to feel really really bad too... like emotionally... something that doesn't seem to dust off easily either... and it's hard to admit.

I feel defeated.

I went out of bed in the early hours of the day to have a walk in the woods today.

And what better soundtrack to have than **Somewhere Along The Highway** by **Cult of Luna**. 

> When the streetlights fade.
 Warm rain like judgement descends.

>Their voice numbs me. Speaking words in a dead tongue.

>I have walked a road that lead me back to you.

>From a window our glances met. My true colours I cannot hide.

>The landscape has changed. 
You don't recognise me.

>These pictures slowly fade.
 Memories wither, they are all gone.

>Further down the steps get steeper. 
You haunt me in my dreams.

>I let go and fall deeper. This will be the end of me.

---

##### Images for todays post:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(28).jpg" gallery="gallery" caption="The landscape has changed." >}}
