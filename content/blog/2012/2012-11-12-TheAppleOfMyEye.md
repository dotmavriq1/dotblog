+++
author = "dotMavriQ"
title = "The Apple of My Eye"
date = "2012-11-12"
description = "Enjoying some Äppelmust from Bramleys"
tags = ["Life"]
categories = ["Blog"]
aliases = ["2012-11-12-Theappleofmyeye"]
removeBlur = false
comments = false

[[images]]
src = "https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram/instagram%20(29).jpg"
alt = ""
stretch = ""
+++

I've got to represent Kålland and what better way to do it than to try the *Äppelmust* from Bramleys!

> Bramleys is the name of the Swedish apple-growing business run by Joacim Holgersson and his English wife Fiona. Why Bramleys? For the simple reason that Fiona grew up on a farm called Bramleys. Also, one of the couple’s favourite apple varieties is Bramleys – a sharp but gently aromatic fruit. Bramleys now have over 17,000 apple trees made up of 100 different varieties, all over the Skaraborg region. Aside from organic apples, the company grows pears and giant blueberries. 

I haven't tried it before and I must say that I am very pleasantly surprised.

Clear character in the apple, it's all nice and cloudy.

And because it's a must, for all you non-Swedish speakers out there, it means that it consists of squeezings of apple and that alone.

This one was really aromatic, I am pleasantly surprised! 

I'll look forward to trying more from them in the future, for sure.

---

##### Images for todays post:


{{< fancybox path="https://s3.eu-central-1.wasabisys.com/dotblog/dotBlog/Blog/Instagram" file="instagram%20(29).jpg" gallery="gallery" caption="So tasty!" >}}
