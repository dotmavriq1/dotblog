+++
title = "About"
description = "μετά"
author = "Hugo Authors"
date = "2020-01-05"
layout = "about"
+++

## The dotBlog

The dotBlog is the definitive blog by [Open Source](https://en.wikipedia.org/wiki/Open-source_software) Advocate and Internet Denizen **dotMavriQ**. 

I will try, to the best of my abilities, to design and structure the website in a way that makes it easy for anyone to navigate the different topics as the blog deals with a range of topics so wide and difficult to define that grander judgments regarding subject matter has to fall in the hands of you or someone other than me.

I could recommend it in particular to those inclined to seek truth and wisdom, understand systems, learn for life, and garden in all possible aspects of it.

**dotMavriQ** is a [handle](https://techterms.com/definition/handle) that I have been using for a good amount of years.

The handle has absolutely nothing to do with the Microsoft .NET framework in any way, shape, or form.

It originates from an affinity I got for so-called [dotfiles](https://en.wikipedia.org/wiki/Hidden_file_and_hidden_directory), or hidden files in a [UNIX](https://en.wikipedia.org/wiki/Unix) system.

Colloquially it is used to describe the files a user tinkers with to customize configuration files for programs that they use to not only maximize performance but also to personalize the tools in the hands of the one that uses them.

The dot is then also a symbol of esoteric and obscurantist inclinations, of all things hidden yet present.

A maverick is of course someone that goes their own way and I would claim that I have throughout my life.

One of the more comedic and poignant recollections my parents have told me is that the question I never stopped asking as a young child was "What does that do?". To me, it becomes telling in a way that things and the relation of things to one another will never cease to interest me. I tend to steer towards a pragmatic and functionalist view of the world more than anything, *I believe*.

Curiosity may have killed the cat, *but it kept me alive*.

And I love learning, with religious levels of fervor, which is why my logo depicts the two ravens, **Huginn** and **Muninn**, *Thought* and *Recollection*.

I blog to remember, to relax, to further reflect, and hopefully to amuse and spark interest among anyone that chooses to read what I write.

---
## Jonatan
My name is Jonatan and I currently reside in Lidköping, a small town in Sweden, where I work and spend time with my partner Margarida, whenever possible.

I work as a [Web Developer](https://en.wikipedia.org/wiki/Web_developer) and Social Media Manager for [Duva](https://duva.nu "Duva"), with experience in managing big ad campaigns and [CRM](https://dynamics.microsoft.com/en-us/crm/what-is-crm/).

My job involves brand strategy, visual design, and account management for a mindful but competitive approach. 

I am fueled by my passion for understanding the nuances of cross-cultural advertising as well as systems comprehension. 

I consider myself an *eternal student* who is always eager to improve my foundations in web development and full stack comprehension as well as staying in tune with the latest digital marketing strategies through continued fieldwork.

My hunger for knowledge and determination to turn information into action has contributed to the creation of the [dotBlog](https://blog.dotMavriQ.Life), where I write anything ranging from helpful guides and business insights as well as tips and tricks of the trade, productivity improvements, and concerns in the field of IT.

When I'm not in the world of programming I engage in my other interests. 

My other interests involve philosophy, reading, travel, and cooking.

## CV

[Link to PDF (Direct Download)](https://s3.eu-central-1.wasabisys.com/dotblog/JonatanJanssonCV.pdf)

<a href="https://stackoverflow.com/story/dotmavriq"><img src="https://stackoverflow.com/users/flair/4709832.png?theme=dark" width="208" height="58" alt="profile for dotmavriq at Stack Overflow, Q&A for professional and enthusiast programmers" title="profile for dotmavriq at Stack Overflow, Q&A for professional and enthusiast programmers"></a>

***
