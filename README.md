## The dotBlog

The dotBlog is the definitive blog by [Open Source](https://en.wikipedia.org/wiki/Open-source_software) Advocate and Internet Denizen **dotMavriQ**. 

I will try, to the best of my abilities, to design and structure the website in a way that makes it easy for anyone to navigate the different topics as the blog deals with a range of topics so wide and difficult to define that grander judgments regarding subject matter has to fall in the hands of you or someone other than me.

I could recommend it in particular to those inclined to seek truth and wisdom, understand systems, learn for life, and garden in all possible aspects of it.

**dotMavriQ** is a [handle](https://techterms.com/definition/handle) that I have been using for a good amount of years.

The handle has absolutely nothing to do with the Microsoft .NET framework in any way, shape, or form.

It originates from an affinity I got for so-called [dotfiles](https://en.wikipedia.org/wiki/Hidden_file_and_hidden_directory), or hidden files in a [UNIX](https://en.wikipedia.org/wiki/Unix) system.

Colloquially it is used to describe the files a user tinkers with to customize configuration files for programs that they use to not only maximize performance but also to personalize the tools in the hands of the one that uses them.

The dot is then also a symbol of esoteric and obscurantist inclinations, of all things hidden yet present.

A maverick is of course someone that goes their own way and I would claim that I have throughout my life.

One of the more comedic and poignant recollections my parents have told me is that the question I never stopped asking as a young child was "What does that do?". To me, it becomes telling in a way that things and the relation of things to one another will never cease to interest me. I tend to steer towards a pragmatic and functionalist view of the world more than anything, *I believe*.

Curiosity may have killed the cat, *but it kept me alive*.

And I love learning, with religious levels of fervor, which is why my logo depicts the two ravens, **Huginn** and **Muninn**, *Thought* and *Recollection*.

I blog to remember, to relax, to further reflect, and hopefully to amuse and spark interest among anyone that chooses to read what I write.

--- 

### TODO: 

- [ ] Complete the theming - Colors 
- [ ] Complete the theming - Fonts
- [ ] Add Archives
- [ ] Add hosted Commento solution, remove Disqus
- [ ] Fix theming issues for Category and About in the sidepanel.
- [ ] Improve the header and share button functionality
- [ ] Share functionality for Fediverse
- [ ] Finalize Swedish and Portuguese language support
- [ ] Create necessary formatting for Recipes